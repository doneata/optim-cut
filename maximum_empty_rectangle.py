import math
import pdb
import random

from enum import Enum

from functools import reduce

from typing import List, Optional, Tuple

import cairo

from toolz import concat, sliding_window


SEED = 1337
random.seed(SEED)


class Degree(Enum):
    D0 = 0
    D90 = 1
    D180 = 2
    D270 = 3

    def complementary(self) -> "Degree":
        if self is Degree.D0:
            return Degree.D0
        elif self is Degree.D90:
            return Degree.D270
        elif self is Degree.D180:
            return Degree.D180
        elif self is Degree.D270:
            return Degree.D90


class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def rotate(self, degree: Degree) -> "Point":
        if degree is Degree.D0:
            return Point(self.x, self.y)
        elif degree is Degree.D90:
            return Point(-self.y, self.x)
        elif degree is Degree.D180:
            return Point(-self.x, -self.y)
        elif degree is Degree.D270:
            return Point(self.y, -self.x)


class Rectangle:
    def __init__(self, left: float, top: float, right: float, bottom: float):
        self.left = left
        self.top = top
        self.right = right
        self.bottom = bottom

    def to_points(self) -> Tuple[Point, Point]:
        return Point(self.left, self.top), Point(self.right, self.bottom)

    @classmethod
    def from_points(cls, p1: Point, p2: Point):
        return cls(
            left=min(p1.x, p2.x),
            top=min(p1.y, p2.y),
            right=max(p1.x, p2.x),
            bottom=max(p1.y, p2.y),
        )

    def rotate(self, degree) -> "Rectangle":
        tl, br = self.to_points()
        tl_rot = tl.rotate(degree)
        br_rot = br.rotate(degree)
        return Rectangle.from_points(tl_rot, br_rot)


def generate_points(num_points: int, rectangle: Rectangle) -> List[Point]:
    get_x = lambda: random.uniform(rectangle.left, rectangle.right)
    get_y = lambda: random.uniform(rectangle.top, rectangle.bottom)
    return [Point(get_x(), get_y()) for _ in range(num_points)]


def get_maximal_rectangles_1(
    rectangle: Rectangle, points: List[Point]
) -> List[Rectangle]:
    # Type 1: two opposite edges coincide with the edge of the rectangle
    xs = [rectangle.left] + sorted(p.x for p in points) + [rectangle.right]
    return [
        Rectangle(x1, rectangle.top, x2, rectangle.bottom)
        for x1, x2 in sliding_window(2, xs)
    ]


def get_maximal_rectangles_2(
    rectangle: Rectangle, points: List[Point]
) -> List[Rectangle]:
    # Type 2: two adjacent edges coincide with the edges of the rectangle
    def reducer(accum, point):
        points, x = accum
        if x is None or point.x < x:
            points1 = points + [point]
            x1 = point.x
        else:
            points1 = points
            x1 = x
        return points1, x1

    init = ([], None)  # type: Tuple[List[Point], Optional[Point]]
    points1, _ = reduce(reducer, sorted(points, key=lambda p: p.y), init)

    return [
        Rectangle(rectangle.left, rectangle.top, p1.x, p2.y)
        for p1, p2 in sliding_window(2, points1)
    ]


def get_maximal_rectangles_3(
    rectangle: Rectangle, points: List[Point]
) -> List[Rectangle]:
    # Type 3: a single edge coincides with the edge of the rectangle
    def find_top_left(point, points) -> List[Point]:
        return [p for p in points if p.x < point.x and p.y < point.y]

    def find_top_right(point, points) -> List[Point]:
        return [p for p in points if p.x > point.x and p.y < point.y]

    tls = [find_top_left(p, points) for p in points]
    trs = [find_top_right(p, points) for p in points]
    return [
        Rectangle(max(l.x for l in tl), rectangle.top, min(r.x for r in tr), p.y)
        for tl, tr, p in zip(tls, trs, points)
        if tl and tr
    ]


def get_maximal_rectangles_4(
    rectangle: Rectangle, points: List[Point]
) -> List[Rectangle]:
    # Type 4: no edge coincides with the edge of the rectangle
    def reducer(accum, point):
        rects, (top_point, left, right) = accum
        # print(left, right, top_point, point.x)
        if top_point is None:
            top_point  = point
        elif left is None and point.x < top_point.x:
            left = point.x
        elif right is None and point.x > top_point.x:
            right = point.x
        elif left is not None and right is not None and left < point.x < right:
            rects = rects + [Rectangle(left, top_point.y, right, point.y)]
            if point.x > top_point.x:
                right = point.x
            else:
                left = point.x
        elif left is not None and left < point.x:
            left = point.x
        elif right is not None and point.x < right:
            right = point.x
        else:
            pass
        return rects, (top_point, left, right)

    n = len(points)
    points1 = sorted(points, key=lambda p: p.y)
    for p in points1:
        print(p.x, p.y)
    print("---")
    init = lambda: ([], (None, None, None))
    return concat(reduce(reducer, points1[i:], init())[0] for i in range(n))


def apply_rotated(func, rectangle, points, degree):
    results = func(rectangle.rotate(degree), [p.rotate(degree) for p in points])
    return [r.rotate(degree.complementary()) for r in results]


def get_maximal_rectangles(
    rectangle: Rectangle, points: List[Point]
) -> List[Rectangle]:
    # Selects the candidates for the maximum empty rectangle. These rectangle
    # are also known as "restricted rectangles"; in the following, I'm using
    # the terminology of Naamad et. al:
    # > Naamad, Amnon, D. T. Lee, and W-L. Hsu. "On the maximum empty rectangle
    # > problem." Discrete Applied Mathematics 8.3 (1984): 267-277.
    return concat(
        [
            # type 1
            apply_rotated(get_maximal_rectangles_1, rectangle, points, Degree.D0),
            apply_rotated(get_maximal_rectangles_1, rectangle, points, Degree.D90),
            # type 2
            apply_rotated(get_maximal_rectangles_2, rectangle, points, Degree.D0),
            apply_rotated(get_maximal_rectangles_2, rectangle, points, Degree.D90),
            apply_rotated(get_maximal_rectangles_2, rectangle, points, Degree.D180),
            apply_rotated(get_maximal_rectangles_2, rectangle, points, Degree.D270),
            # type 3
            apply_rotated(get_maximal_rectangles_3, rectangle, points, Degree.D0),
            apply_rotated(get_maximal_rectangles_3, rectangle, points, Degree.D90),
            apply_rotated(get_maximal_rectangles_3, rectangle, points, Degree.D180),
            apply_rotated(get_maximal_rectangles_3, rectangle, points, Degree.D270),
            # type 4
            apply_rotated(get_maximal_rectangles_4, rectangle, points, Degree.D0),
        ]
    )


def plot(
    rectangle: Rectangle, points: List[Point], candidates: List[Rectangle]
) -> None:
    PIXEL_SCALE = 100

    width = rectangle.right - rectangle.left
    height = rectangle.bottom - rectangle.top

    # Set up the canvas
    surface = cairo.ImageSurface(
        cairo.FORMAT_ARGB32, int(width * PIXEL_SCALE), int(height * PIXEL_SCALE)
    )
    ctx = cairo.Context(surface)
    ctx.scale(PIXEL_SCALE, PIXEL_SCALE)  # Normalizing the canvas

    # Draw rectangle
    ctx.rectangle(0, 0, width, height)
    ctx.set_source_rgb(0.8, 0.8, 1)
    ctx.fill()

    # Draw points
    for point in points:
        ctx.save()
        ctx.translate(point.x, point.y)
        ctx.set_source_rgb(1, 0, 0)
        ctx.arc(0, 0, 0.1, 0, 2 * math.pi)
        ctx.fill()
        ctx.select_font_face("Sans")
        ctx.set_font_size(0.15)
        ctx.translate(-0.3, -0.15)
        ctx.set_source_rgba(0.65, 0, 0, 0.3)
        ctx.show_text("({:.1f}, {:.1f})".format(point.x, point.y))
        ctx.restore()

    # Draw candidates
    ctx.set_source_rgba(0, 0, 0, 0.2)
    ctx.set_line_width(0.04)

    for r in candidates:
        ctx.save()
        ctx.rectangle(r.left, r.top, r.right - r.left, r.bottom - r.top)
        ctx.stroke()
        ctx.restore()

    surface.write_to_png("example.png")


def main():
    num_points = 5
    rectangle = Rectangle(0, 0, 10, 3)
    points = generate_points(num_points, rectangle)
    candidates = get_maximal_rectangles(rectangle, points)
    plot(rectangle, points, candidates)


if __name__ == "__main__":
    main()
