"""Data structures"""

from enum import Enum

from typing import Dict, List, NamedTuple, Optional, Tuple

from pint import Quantity  # type: ignore

import portion as P  # type: ignore

from constants import U


class Size(NamedTuple):
    width: Quantity
    height: Quantity


class Degree(Enum):
    D0 = 0
    D90 = 1
    D180 = 2
    D270 = 3

    def complementary(self) -> "Degree":
        if self is Degree.D0:
            return Degree.D0
        elif self is Degree.D90:
            return Degree.D270
        elif self is Degree.D180:
            return Degree.D180
        elif self is Degree.D270:
            return Degree.D90


class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def rotate(self, degree: Degree) -> "Point":
        if degree is Degree.D0:
            return Point(self.x, self.y)
        elif degree is Degree.D90:
            return Point(-self.y, self.x)
        elif degree is Degree.D180:
            return Point(-self.x, -self.y)
        elif degree is Degree.D270:
            return Point(self.y, -self.x)

    def translate(self, dx: float, dy: float) -> "Point":
        return Point(self.x + dx, self.y + dy)

    def __str__(self) -> str:
        return "({:.2f}, {:.2f})".format(self.x, self.y)

    def __repr__(self) -> str:
        return "Point(x={}, y={})".format(self.x, self.y)

    def __eq__(self, other: object) -> bool:
        if isinstance(other, Point):
            return self.x == other.x and self.y == other.y
        else:
            return NotImplemented


class Rectangle:
    def __init__(self, left: float, top: float, right: float, bottom: float):
        self.left = left
        self.top = top
        self.right = right
        self.bottom = bottom
        assert self.left <= self.right
        assert self.top <= self.bottom

    def to_points(self) -> Tuple[Point, Point]:
        return Point(self.left, self.top), Point(self.right, self.bottom)

    def to_corners(self) -> Tuple[Point, Point, Point, Point]:
        """Returns all four corners."""
        return (
            Point(self.left, self.top),
            Point(self.left, self.bottom),
            Point(self.right, self.top),
            Point(self.right, self.bottom),
        )

    @classmethod
    def from_points(cls, p1: Point, p2: Point) -> "Rectangle":
        return cls(
            left=min(p1.x, p2.x),
            top=min(p1.y, p2.y),
            right=max(p1.x, p2.x),
            bottom=max(p1.y, p2.y),
        )

    def rotate(self, degree: Degree) -> "Rectangle":
        tl, br = self.to_points()
        tl_rot = tl.rotate(degree)
        br_rot = br.rotate(degree)
        return Rectangle.from_points(tl_rot, br_rot)

    def translate(self, dx: float, dy: float) -> "Rectangle":
        tl, br = self.to_points()
        tl_trans = tl.translate(dx, dy)
        br_trans = br.translate(dx, dy)
        return Rectangle.from_points(tl_trans, br_trans)

    def to_dict(self) -> Dict[str, float]:
        return {
            "left": self.left,
            "right": self.right,
            "top": self.top,
            "bottom": self.bottom,
        }

    def to_interval_x(self, type1: str = "closed") -> P.Interval:
        if type1 == "closed":
            return P.closed(self.left, self.right)
        else:
            return P.open(self.left, self.right)

    def to_interval_y(self, type1: str = "closed") -> P.Interval:
        if type1 == "closed":
            return P.closed(self.top, self.bottom)
        else:
            return P.open(self.top, self.bottom)

    def to_size(self, resolution: Quantity) -> Size:
        width = (self.width * resolution).to(U.foot)
        height = (self.height * resolution).to(U.inch)
        return Size(width, height)

    def area(self) -> float:
        return abs((self.bottom - self.top) * (self.right - self.left))

    def overlaps(self, other: "Rectangle", type1: str = "closed") -> bool:
        overlaps_x = self.to_interval_x(type1).overlaps(other.to_interval_x(type1))
        overlaps_y = self.to_interval_y(type1).overlaps(other.to_interval_y(type1))
        return overlaps_x and overlaps_y

    @property
    def width(self) -> float:
        return self.right - self.left

    @property
    def height(self) -> float:
        return self.bottom - self.top

    def __str__(self) -> str:
        return "({:.2f}, {:.2f}, {:.2f}, {:.2f})".format(
            self.left, self.top, self.right, self.bottom
        )

    def __eq__(self, other: object) -> bool:
        if isinstance(other, Rectangle):
            return (
                self.left == other.left
                and self.top == other.top
                and self.right == other.right
                and self.bottom == other.bottom
            )
        else:
            return NotImplemented

    def __repr__(self) -> str:
        return "Rectangle(left={}, top={}, right={}, bottom={})".format(
            self.left, self.top, self.right, self.bottom
        )

    def __contains__(self, other: "Rectangle") -> str:
        contained_x = self.to_interval_x().contains(other.to_interval_x())
        contained_y = self.to_interval_y().contains(other.to_interval_y())
        return contained_x and contained_y

    def __hash__(self):
        return hash((self.left, self.top, self.right, self.bottom))


class Side(Enum):
    FRONT = 1
    BACK = 2


class DefectType(Enum):
    # Use the UGRS data format; see Table A1 from link below
    # https://www.fs.fed.us/ne/newtown_square/publications/technical_reports/pdfs/1998/gtr254.pdf
    STAIN = 1
    CHECKS = 2
    SOUND_KNOT = 3
    UNSOUND_KNOT = 4
    WANE = 5
    SPLIT_OR_SHAKE = 6
    PITH = 7
    HOLE = 8
    DECAY = 9
    BARK_POCKET = 10
    VOID = 11


class Defect(NamedTuple):
    shape: Rectangle
    type_: Optional[DefectType] = None
    side: Optional[Side] = None


class Board:
    def __init__(self, shape: Rectangle, defects: List[Defect], resolution: Quantity):
        self.shape = shape
        self.defects = defects
        self.resolution = resolution  # number of units per pixels

    def to_size(self) -> Size:
        return self.shape.to_size(self.resolution)
