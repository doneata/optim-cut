import argparse
import json
import pdb
import os
import time
import warnings

from typing import Any, Callable, Dict, List, Literal, Optional, TypedDict

from functools import wraps
from itertools import count

import numpy as np

from board import Side, U

from candidates import (
    Rectangle,
    get_maximal_rectangles,
    get_maximal_rectangles_selection,
    plot,
)

from data import DATASETS, split_board

from grading import (
    GRADES,
    GRADES_1_ORDER,
    get_cutting_units,
    get_max_grade_2_from_grades,
    get_surface_measure,
)

from methods import METHODS

from cut import SCORING_FUNC


# Some types
GradeLiteral = Literal["FAS", "F1F", "SEL", "No1COM", "No2ACOM", "No3ACOM", "NULL"]
DataSide = TypedDict("DataSide", {"max-grade": GradeLiteral, "grades-info": List[Any]})


SUBSET_TO_GRADE = {
    "fas": "FAS",
    "f1f": "F1F",
    "sel": "SEL",
    "1c": "No1COM",
    "2ac": "No2ACOM",
    "3ac": "No3ACOM",
}  #  type: Dict[str, GradeLiteral]


def get_from_cached(
    results, board_name: str, side: str, grade: str
) -> Optional[List[Rectangle]]:
    result, *_ = [
        result["side-data"][side]["grades-info"]
        for result in results
        if result["board-name"] == board_name
    ]
    result = [r for r in result if r["grade"] == grade]
    if len(result) == 0:
        return None
    else:
        return [Rectangle(**d) for d in result[0]["selected-cuts"]]


def main():
    selected_datasets = [dataset for dataset in DATASETS if dataset.startswith("ro98")]
    parser = argparse.ArgumentParser(description="Cut based on a specified method")
    parser.add_argument(
        "-m", "--method", type=str, choices=METHODS, help="which cutting method to use"
    )
    parser.add_argument(
        "-d",
        "--dataset",
        type=str,
        choices=selected_datasets,
        help="on which data to run",
    )
    parser.add_argument(
        "-n",
        "--num-boards",
        type=int,
        help="how many images from the dataset to process",
    )
    parser.add_argument(
        "-o", "--outdir", default="output", help="where to store the images and results"
    )
    parser.add_argument("-s", "--min-size", help="minimum size (e.g., 10x3)")
    parser.add_argument(
        "--score",
        choices=SCORING_FUNC,
        default="area",
        help="which score function to use",
    )
    parser.add_argument(
        "--dry-run",
        default=False,
        action="store_true",
        help="run without overwriting previously saved results",
    )
    parser.add_argument("--cached-results", help="partial solution")
    parser.add_argument(
        "-v", "--verbose", default=0, action="count", help="verbosity level"
    )
    args = parser.parse_args()

    dataset = DATASETS[args.dataset]()
    num_boards = args.num_boards or len(dataset)
    scoring_function = SCORING_FUNC[args.score]

    imgdir = os.path.join(
        args.outdir, "images", f"{args.dataset}-{args.method}-{args.score}"
    )
    os.makedirs(imgdir, exist_ok=True)

    data = []

    # fmt: off
    needs_init_data = (
        args.method.startswith("ipqp") and
        "init:greedy" in args.method.split("-")
    )
    # fmt: on
    assert not needs_init_data, "Not implemented"

    dataset_name, subset_grade = args.dataset.split("-")
    assert dataset_name == "ro98", "Unknow dataset"

    graders = [GRADES[n] for n in GRADES_1_ORDER]
    for g in graders:
        g.is_kiln_dried = True

    if args.cached_results:
        with open(args.cached_results, "r") as f:
            cached_results = json.load(f)

    for i in range(num_boards):
        board = dataset[i]
        board_size = board.to_size()
        data_sides = {
            "FRONT": {
                "max-grade": "NULL",
                "grades-info": [],
            },
            "BACK": {
                "max-grade": "NULL",
                "grades-info": [],
            },
        }  # type: Dict[str, DataSide]

        # if i != 49:
        #     continue

        # if dataset.board_names[i] not in {"0076"}:
        #     continue

        for side in Side:

            # if side.name != "FRONT":
            #     continue

            extra = {}  # type: Dict[str, Any]
            defects = [defect for defect in board.defects if defect.side == side]
            defects_shapes = [defect.shape for defect in defects]

            if args.method == "manual-selection":
                extra["board_index"] = i
                extra["side"] = side.name

            for grader in graders:

                if not grader.has_min_board_size(board_size):
                    data_sides[side.name]["grades-info"].append(
                        {
                            "grade": grader.name,
                            "has-min-size": False,
                            "is-grade": False,
                            "selected-cuts": [],
                        }
                    )
                    continue

                extra["min_sizes"] = [
                    (
                        (size.height.to(U.inch) / board.resolution).magnitude,
                        (size.width.to(U.inch) / board.resolution).magnitude,
                    )
                    for size in grader.min_cut_sizes
                ]

                if args.method.startswith("exhaustive"):
                    sm = get_surface_measure(board_size)
                    area = sm.magnitude / (
                        board.resolution.to(U.inch).magnitude
                        * board.resolution.to(U.foot).magnitude
                    )
                    extra["required_areas"] = [grader.yield_factor * area]
                    extra["num_cuts"] = grader.get_num_cuts(board_size)
                    if grader.allows_extra_cut(sm):
                        extra["required_areas"].append(grader.yield_factor_extra * area)
                        # extra["num_cuts"] += 1

                def has_min_size(rect):
                    return grader.has_min_cut_size(rect.to_size(board.resolution))

                selected1 = args.cached_results and get_from_cached(
                    cached_results, dataset.board_names[i], side.name, grader.name
                )

                time_s = time.time()
                if args.cached_results and selected1 is None:
                    selected1 = METHODS[args.method](
                        board.shape,
                        defects_shapes,
                        has_min_size=has_min_size,
                        scoring_function=scoring_function,
                        verbose=args.verbose,
                        **extra,
                    )
                time_e = time.time()

                cuts = [s.to_size(board.resolution) for s in selected1]

                has_grade, info = grader.is_grade_1(
                    board, cuts, defects, to_return_extra=True
                )

                num_selected = len(selected1)
                is_overlap = any(
                    selected1[m].overlaps(selected1[n], type1="semiclosed")
                    for m in range(num_selected)
                    for n in range(m + 1, num_selected)
                )

                if is_overlap:
                    warnings.warn("the selected candidates overlap")
                    pdb.set_trace()

                img_path = os.path.join(
                    imgdir, f"{i:04d}-{side.name}-{grader.name}.png"
                )

                if not args.dry_run:
                    plot(board.shape, defects_shapes, selected1, outpath=img_path)

                num_cuts = info["num-cuts"]
                selected_cuts = sorted(selected1, key=lambda s: s.area(), reverse=True)
                selected_cuts = selected_cuts[:num_cuts]
                selected_cuts = [s.to_dict() for s in selected_cuts]
                data_sides[side.name]["grades-info"].append(
                    {
                        "grade": grader.name,
                        "img-path": img_path,
                        "is-overlap": is_overlap,
                        "is-grade": has_grade,
                        "selected-cuts": selected_cuts,
                        "time": time_e - time_s,
                    }
                )
                data_sides[side.name]["grades-info"][-1].update(info)

                if has_grade:
                    data_sides[side.name]["max-grade"] = grader.name
                    break

        max_grade_2 = get_max_grade_2_from_grades(
            [
                data_sides["FRONT"]["max-grade"],
                data_sides["BACK"]["max-grade"],
            ]
        )

        datum = {
            "board-index": i,
            "board-name": dataset.board_names[i],
            "max-grade": max_grade_2,
            "side-data": data_sides,
        }
        data.append(datum)
        # pdb.set_trace()

        print(json.dumps(datum, indent=True))

    if not args.dry_run:
        path = os.path.join(
            args.outdir, "results", f"{args.dataset}-{args.method}-{args.score}.json"
        )
    else:
        path = "/tmp/tmp.json"

    with open(path, "w") as f:
        json.dump(data, f, indent=True, sort_keys=True)


if __name__ == "__main__":
    main()
