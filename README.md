## Code

Set up:

```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

Run tests:

```bash
python -m unittest
```

## Task

Given:

- a wooden _board_ (defined in terms of height and width)
- a list of detected _defects_ (defined as boxes---location, height and width)
- a set of _constraints_ (specified as three types: quality, shape, defects)

we want to determine plank sizes and their locations such that

- the planks do not overlap
- the planks obey all the constraints
- the waste is minimized (or, alternatively, the covered area is maximized).

### Constraints

- **Quality** can be of the following:
FAS, FAS One Face (F1F), Selects, No. 1 Common, No. 2A Common, No. 3A Common, No. 2B Common, No. 3B Common, Sound Wormy.
The quality constraints are rather intricate and might involve solving an inner optimization problem.

- **Size** specifies a constraint on height, width or both.
The user can select either exact or minimum size.

- **Defects** imposes a maximum number of appearances for a list of defects.
The constraints on defects can be specified as a list of tuples:
maximum number of occurrences and the type of defect. 
The types of defects can be one of the following.

### On grading

Glossary of the most important terms:

```bash
cutting unit ←  1 inch × 1 foot
clear units ←  width (inches) × length (feet)
board foot ←  12 inches × 12 inches × 1 inch
surface measure (SM) ←  int(width (inches) × length (feet) / 12)
```

## Data

We will start the task by using generated (synthetic) data.
Ideally, we want the synthetic data to resemble the ground-truth as much as possible.

Here are the distributions that I need to define in order to generate data:

- Board: size.
- Plank: sizes
- Defects: location, size, type.

## Methods

In this section I briefly present the main algorithms.

**Greedy algorithm.**
At each iteration:

1. generate all the maximal rectangles that do not contain any defects and any previously selected cuts
2. increase the list of selected cuts with the largest rectangles

Another formulation could be based only on the list of initial candidates.
This is faster, but it doesn't cover as well the board.

## To do

- [ ] Generate synthetic data
- [ ] Document regarding optimization algorithm

**Check if board has FAS quality.**

- Create tests based on online material
[1](https://www.americanhardwood.org/sites/default/files/publications/download/2017-10/AHEC%20Grading%20Guide_AW_pages_0.pdf)
[2](https://www.youtube.com/watch?v=NukMeFj99Gc)
- Write function to plot a board
- Write function to serialize and de-serialize board to JSON

## References

Papers on the maximum rectangle problem:

- Naamad, Amnon, D. T. Lee, and W-L. Hsu. "On the maximum empty rectangle problem." Discrete Applied Mathematics 8.3 (1984): 267-277.
- Overmars, Mark H., and Derick Wood. "On rectangular visibility." Journal of Algorithms 9.3 (1988): 372-390.
- Atallah, Mikhail J., and S. Rao Kosaraju. "An efficient algorithm for maxdominance, with applications." Algorithmica 4.1-4 (1989): 221-236.
- Orlowski, M. "A new algorithm for the largest empty rectangle problem." Algorithmica 5.1-4 (1990): 65-73.
- Datta, Amitava. "Efficient algorithms for the largest rectangle problem." Information sciences 64.1-2 (1992): 121-141.

Related papers on defect cutting or different types of obstacles:

- Nandy, Subhas C., Bhargab B. Bhattacharya, and Sibabrata Ray. "Efficient algorithms for identifying all maximal isothetic empty rectangles in VLSI layout design." International Conference on Foundations of Software Technology and Theoretical Computer Science. Springer, Berlin, Heidelberg, 1990.
- Nandy, Subhas C., Arani Sinha, and Bhargab B. Bhattacharya. "Location of the largest empty rectangle among arbitrary obstacles." International Conference on Foundations of Software Technology and Theoretical Computer Science. Springer, Berlin, Heidelberg, 1994.
- Twisselmann, U. "Cutting rectangles avoiding rectangular defects." Applied mathematics letters 12.6 (1999): 135-138.
- Afsharian, Mohsen, Ali Niknejad, and Gerhard Wäscher. "A heuristic, dynamic programming-based approach for a two-dimensional cutting problem with defects." OR spectrum 36.4 (2014): 971-999.

Papers on evoluationay algorithms:

- [The class cover problem with boxes](https://www.sciencedirect.com/science/article/pii/S0925772112000375)
- [Approximation Algorithms for Rectangle Packing Problems](https://arxiv.org/pdf/1711.07851.pdf)
- [Two-dimensional Packing utilising Evolutionary Algorithms and other Meta-Heuristic Methods](https://www.inf.utfsm.cl/~mcriff/EA/eva-space-planning/part1.pdf)
- [Randomization and Approximation Techniques in Computer Science](https://books.google.ro/books?id=Fd4GQPhoDEUC&pg=PA28&lpg=PA28&dq=covering+vlsi+with+minimum+rectangles&source=bl&ots=jQAfe-QLml&sig=ACfU3U2VKrLJfqZiofrNr87M96aovhfPKg&hl=en&sa=X&ved=2ahUKEwiq6--I8cnoAhWGtIsKHROdDKoQ6AEwBXoECAsQLg#v=onepage&q=covering%20vlsi%20with%20minimum%20rectangles&f=false)
