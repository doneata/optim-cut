from enum import Enum

from typing import Any, Callable, Dict, List, Literal

from pint import Quantity  # type: ignore

from board import Board, Defect, DefectType, Size


AggType = Literal["sum", "max"]
SizeType = Literal["length", "diameter"]


AGG_FUNCS = {
    "sum": sum,
    "max": max,
}  # type: Dict[AggType, Callable[..., float]]


GET_SIZE_FUNCS = {
    "length": lambda s: s.width,
    "diameter": lambda s: (s.width + s.height) / 2,
}  # type: Dict[SizeType, Callable[[Size], Quantity]]


class DefectLimits:
    def __init__(
        self,
        defect_type: DefectType,
        aggregation: AggType,
        size_type: SizeType,
        has_allowed_size: Callable[[Quantity, Size], bool],
    ):
        self.defect_type = defect_type
        self._agg_func = AGG_FUNCS[aggregation]
        self._get_size = GET_SIZE_FUNCS[size_type]
        self.has_allowed_size = has_allowed_size

    def within_limits(self, defects: List[Defect], board: Board) -> bool:
        defect_sizes = [
            self._get_size(defect.shape.to_size(board.resolution))
            for defect in defects
            if defect.type_ == self.defect_type
        ]
        is_empty = len(defect_sizes) == 0
        return is_empty or self.has_allowed_size(self._agg_func(defect_sizes), board.to_size())
