import argparse
import math
import pdb

from functools import reduce

from itertools import product

from typing import Any, Callable, Dict, List, Optional, Tuple

import portion as P  # type: ignore

from toolz import concat, sliding_window

from board import Degree, Rectangle, Point

from data import DATASETS, plot


def keep_non_empty(rectangles: List[Rectangle]) -> List[Rectangle]:
    return [r for r in rectangles if r.area() > 0]


def get_maximal_rectangles_1(
    rectangle: Rectangle, defects: List[Rectangle]
) -> List[Rectangle]:
    limits = P.open(rectangle.left, rectangle.right)
    for defect in defects:
        limits -= defect.to_interval_x()
    return [
        Rectangle(limit.lower, rectangle.top, limit.upper, rectangle.bottom)
        for limit in list(limits)
        if not limit.empty
    ]


def get_maximal_rectangles_2(
    rectangle: Rectangle, defects: List[Rectangle]
) -> List[Rectangle]:
    # Type 2: two adjacent edges coincide with the edges of the rectangle
    def reducer(accum: Tuple[List[Rectangle], Optional[float]], defect: Rectangle):
        defects, x = accum
        if x is None or defect.left < x:
            defects1 = defects + [defect]
            x1 = defect.left
        else:
            defects1 = defects
            x1 = x
        return defects1, x1

    init = ([], None)  # type: Tuple[List[Rectangle], Optional[float]]
    defects1, _ = reduce(reducer, sorted(defects, key=lambda d: d.top), init)

    return keep_non_empty(
        [
            Rectangle(rectangle.left, rectangle.top, d1.left, d2.top)
            for d1, d2 in sliding_window(2, defects1)
        ]
    )


def get_maximal_rectangles_3(
    rectangle: Rectangle, defects: List[Rectangle]
) -> List[Rectangle]:
    # Type 3: a single edge coincides with the edge of the rectangle
    def find_limits(defect, defects) -> List[P.Interval]:
        limits = [P.open(-P.inf, P.inf)]
        for d in defects:
            if d.top < defect.top:
                limits = concat(list(limit - d.to_interval_x()) for limit in limits)
                limits = [
                    limit for limit in limits if limit.overlaps(defect.to_interval_x())
                ]
        return limits

    limitss = [find_limits(d, defects) for d in defects]

    return [
        Rectangle(limit.lower, rectangle.top, limit.upper, defect.top)
        for limits, defect in zip(limitss, defects)
        for limit in limits
        if -P.inf < limit.lower
        and limit.upper < P.inf
        and limit.overlaps(defect.to_interval_x())
    ]


def get_maximal_rectangles_4(
    rectangle: Rectangle, defects: List[Rectangle]
) -> List[Rectangle]:
    # Type 4: no edge coincides with the edge of the rectangle
    def reducer(accum, defect):
        rects, (top_defect, limits) = accum
        rects1 = []
        if defect.bottom > top_defect.bottom:
            rects1 = keep_non_empty(
                [
                    Rectangle(
                        left=limit.lower,
                        top=top_defect.bottom,
                        right=limit.upper,
                        bottom=defect.top,
                    )
                    for limit in limits
                    if -P.inf < limit.lower
                    and limit.upper < P.inf
                    and limit.overlaps(defect.to_interval_x())
                    and top_defect.bottom <= defect.top
                ]
            )
            limits = concat(list(limit - defect.to_interval_x()) for limit in limits)
            limits = [
                limit for limit in limits if limit.overlaps(top_defect.to_interval_x())
            ]
        return rects + rects1, (top_defect, limits)

    n = len(defects)
    I = P.closed(-P.inf, P.inf)
    init = lambda d: (
        [],
        (d, [I]),
    )  # type: Callable[[Rectangle], Tuple[List[Rectangle], Tuple[Rectangle, List[Any]]]]
    defects1 = sorted(defects, key=lambda d: d.top)
    return concat(
        reduce(reducer, defects1[:i] + defects1[i + 1 :], init(d))[0]
        for i, d in enumerate(defects1)
    )


def apply_rotated(func, rectangle: Rectangle, defects: List[Rectangle], degree: Degree):
    results = func(rectangle.rotate(degree), [d.rotate(degree) for d in defects])
    return [r.rotate(degree.complementary()) for r in results]


def get_maximal_rectangles(
    board: Rectangle, defects: List[Rectangle]
) -> List[Rectangle]:
    # Selects the candidates for the maximum empty rectangle. These rectangle
    # are also known as "restricted rectangles"; in the following, I'm using
    # the terminology of Naamad et. al:
    # > Naamad, Amnon, D. T. Lee, and W-L. Hsu. "On the maximum empty rectangle
    # > problem." Discrete Applied Mathematics 8.3 (1984): 267-277.
    return concat(
        [
            # type 1
            apply_rotated(get_maximal_rectangles_1, board, defects, Degree.D0),
            apply_rotated(get_maximal_rectangles_1, board, defects, Degree.D90),
            # type 2
            apply_rotated(get_maximal_rectangles_2, board, defects, Degree.D0),
            apply_rotated(get_maximal_rectangles_2, board, defects, Degree.D90),
            apply_rotated(get_maximal_rectangles_2, board, defects, Degree.D180),
            apply_rotated(get_maximal_rectangles_2, board, defects, Degree.D270),
            # type 3
            apply_rotated(get_maximal_rectangles_3, board, defects, Degree.D0),
            apply_rotated(get_maximal_rectangles_3, board, defects, Degree.D90),
            apply_rotated(get_maximal_rectangles_3, board, defects, Degree.D180),
            apply_rotated(get_maximal_rectangles_3, board, defects, Degree.D270),
            # type 4
            apply_rotated(get_maximal_rectangles_4, board, defects, Degree.D0),
        ]
    )


def get_maximal_rectangles_selection(
    board: Rectangle, defects: List[Rectangle], types: List[str]
) -> List[Rectangle]:
    c = []

    if "1a" in types:
        c += apply_rotated(get_maximal_rectangles_1, board, defects, Degree.D0)
    if "1b" in types:
        c += apply_rotated(get_maximal_rectangles_1, board, defects, Degree.D90)

    if "2a" in types:
        c += apply_rotated(get_maximal_rectangles_2, board, defects, Degree.D0)
    if "2b" in types:
        c += apply_rotated(get_maximal_rectangles_2, board, defects, Degree.D90)
    if "2c" in types:
        c += apply_rotated(get_maximal_rectangles_2, board, defects, Degree.D180)
    if "2d" in types:
        c += apply_rotated(get_maximal_rectangles_2, board, defects, Degree.D270)

    if "3a" in types:
        c += apply_rotated(get_maximal_rectangles_3, board, defects, Degree.D0)
    if "3b" in types:
        c += apply_rotated(get_maximal_rectangles_3, board, defects, Degree.D90)
    if "3c" in types:
        c += apply_rotated(get_maximal_rectangles_3, board, defects, Degree.D180)
    if "3d" in types:
        c += apply_rotated(get_maximal_rectangles_3, board, defects, Degree.D270)

    if "4a" in types:
        c += apply_rotated(get_maximal_rectangles_4, board, defects, Degree.D0)

    return c


def get_maximal_rectangles_selection_naive(
    board: Rectangle, defects: List[Rectangle], types: List[str]
) -> List[Rectangle]:
    """Naïve method of generating candidates by enumerating all posibilities."""
    contains_defect = lambda r: any(r.overlaps(defect, "open") for defect in defects)
    lefts = [defect.right for defect in defects] + [board.left]
    tops = [defect.bottom for defect in defects] + [board.top]
    rights = [defect.left for defect in defects] + [board.right]
    bottoms = [defect.top for defect in defects] + [board.bottom]
    rectangles = [
        Rectangle(l, t, r, b)
        for l, t, r, b in product(lefts, tops, rights, bottoms)
        if l <= r and t <= b and not contains_defect(Rectangle(l, t, r, b))
    ]
    is_maximal = lambda r: not any(r in q for q in rectangles if q != r)
    return [r for r in rectangles if r.area() > 0 and is_maximal(r)]


def generate_candidates_from_overlap_inter(
    board, defects, box1: Rectangle, box2: Rectangle, *args, **kwargs
) -> List[Rectangle]:
    lefts = [box1.left, box1.right, box2.left, box2.right]
    rights = lefts
    tops = [box1.top, box1.bottom, box2.top, box2.bottom]
    bottoms = tops
    candidates = [
        Rectangle(l, t, r, b)
        for l, t, r, b in product(lefts, tops, rights, bottoms)
        if l <= r
        and t <= b
        and (Rectangle(l, t, r, b) in box1 or Rectangle(l, t, r, b) in box2)
        and (Rectangle(l, t, r, b).area() > 0)
    ]
    # plot(board, defects, [box1, box2], outpath=f"/tmp/exhaustive/a.png", to_show_coordinates=False)
    # for i, c in enumerate(set(candidates)):
    #     print(i, c)
    #     plot(board, defects, [c], outpath=f"/tmp/exhaustive/board-candidate-{i:03d}.png", to_show_coordinates=False)
    # pdb.set_trace()
    return candidates


def generate_candidates_from_overlap_minsize(
    board, defects, box1: Rectangle, box2: Rectangle, min_sizes: List[Any]
) -> List[Rectangle]:
    def cut_v(box1, box2, x):
        c = []
        if box1.left < box2.left:
            b1, b2 = box1, box2
        else:
            b2, b1 = box1, box2
        if b1.left < x <= b1.right:
            c.append(Rectangle(b1.left, b1.top, x, b1.bottom))
        if b2.left <= x < b2.right:
            c.append(Rectangle(x, b2.top, b2.right, b2.bottom))
        return c

    def cut_h(box1, box2, y):
        c = []
        if box1.top < box2.top:
            b1, b2 = box1, box2
        else:
            b2, b1 = box1, box2
        if b1.top < y <= b1.bottom:
            c.append(Rectangle(b1.left, b1.top, b1.right, y))
        if b2.top <= y < b2.bottom:
            c.append(Rectangle(b2.left, y, b2.right, b2.bottom))
        return c

    # cond1 = (
    #     box1.to_interval_x() in box2.to_interval_x()
    #     and box2.to_interval_y() in box1.to_interval_y()
    # )
    # cond2 = (
    #     box2.to_interval_x() in box1.to_interval_x()
    #     and box1.to_interval_y() in box2.to_interval_y()
    # )
    # is_classification_2 = cond1 or cond2
    # plot(board, defects, [box1, box2], outpath="/tmp/overlap-candidates.png", to_show_coordinates=True)
    candidates = []

    # Vertical split
    for _, min_width in min_sizes:
        xs = [
            min(box1.left, box2.left) + min_width,
            max(box1.left, box2.left),
            max(box1.right, box2.right) - min_width,
            min(box1.right, box2.right),
        ]
        for x in xs:
            candidates.extend(cut_v(box1, box2, x))

    # Horizontal split
    for min_height, _ in min_sizes:
        ys = [
            min(box1.top, box2.top) + min_height,
            max(box1.top, box2.top),
            max(box1.bottom, box2.bottom) - min_height,
            min(box1.bottom, box2.bottom),
        ]
        for y in ys:
            candidates.extend(cut_h(box1, box2, y))

    # # TODO
    # contains_defect = lambda r: any(r.overlaps(defect, "open") for defect in defects)
    # for c in candidates:
    #     try:
    #         assert not contains_defect(c)
    #     except:
    #         plot(board, defects, [box1, box2], outpath=f"/tmp/i.png", to_show_coordinates=False)
    #         plot(board, defects, [c], outpath=f"/tmp/o.png", to_show_coordinates=False)
    #         pdb.set_trace()

    return candidates


def extend_candidates(type1, board, defects, candidates, min_sizes, has_min_size):
    if type1 is None:
        return candidates
    else:
        GENERATING_FUNCS = {
            "inter": generate_candidates_from_overlap_inter,
            "msize": generate_candidates_from_overlap_minsize,
        }  # type: Dict[str, Callable[..., List[Rectangle]]]
        generate_new_candidates = GENERATING_FUNCS[type1]
        n_candidates = len(candidates)
        new_candidates = concat(
            generate_new_candidates(
                board, defects, candidates[i], candidates[j], min_sizes
            )
            for i in range(n_candidates)
            for j in range(i + 1, n_candidates)
            if candidates[i].overlaps(candidates[j])
        )
        new_candidates = [c for c in new_candidates if has_min_size(c)]
        candidates.extend(new_candidates)
        # for i, c in enumerate(set(new_candidates)):
        #     print(i, c)
        #     plot(board, defects, [c], outpath=f"/tmp/exhaustive/board-candidate-{i:03d}.png", to_show_coordinates=False)
        # pdb.set_trace()
        return candidates


def main():
    parser = argparse.ArgumentParser(
        description="Generate and show maximal empty rectangles (MERs)"
    )
    parser.add_argument(
        "-c",
        "--candidates-types",
        type=str,
        nargs="+",
        default="1a 1b 2a 2b 2c 2d 3a 3b 3c 3d 4a".split(),
        help="only certain types of MERs",
    )
    parser.add_argument(
        "-o", "--outpath", default="example.png", help="where to save the image"
    )
    args = parser.parse_args()

    to_set = lambda cs: set([repr(c) for c in cs])

    dataset = DATASETS["synth"]()
    for i in range(len(dataset)):
        board = dataset[i]

        rectangle = board.shape
        defects = [defect.shape for defect in board.defects]

        candidates1 = get_maximal_rectangles_selection(
            rectangle, defects, args.candidates_types
        )
        candidates2 = get_maximal_rectangles_selection_naive(
            rectangle, defects, args.candidates_types
        )
        print(i)
        plot(rectangle, defects, candidates2, args.outpath)
        try:
            assert to_set(candidates1) == to_set(candidates2)
        except AssertionError:
            pdb.set_trace()


if __name__ == "__main__":
    main()
