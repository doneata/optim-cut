import json
import os
import pdb

from PIL import Image  # type: ignore


BASEPATH = "/Volumes/researchDev$/oulu_dataset/salum_wood_dataset"
BOARD_SIDES = list("lrps")


def get_file_names():
    return [
        os.path.splitext(f)[0]
        for f in os.listdir(BASEPATH)
        if os.path.splitext(f)[1] == ".txt"
    ]


def get_board_size(file_name):
    with Image.open(os.path.join(BASEPATH, file_name + ".png")) as img:
        return img.size


def get_defects(file_name):
    def parse(line):
        x1, y1, x2, y2, _ = map(int, line.split())
        return x1, y1, x2, y2

    with open(os.path.join(BASEPATH, file_name + ".txt"), "r") as f:
        return [parse(line) for line in f.readlines()]


def main():
    data = [
        {"name": f, "board-size": get_board_size(f), "defects": get_defects(f)}
        for f in get_file_names()
    ]
    path = "data/salum.json"
    with open(path, "w") as f:
        json.dump(data, f, indent=True, sort_keys=False)


if __name__ == "__main__":
    main()
