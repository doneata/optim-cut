import os
import pdb
import sys
import shutil

from pprint import pprint

import dominate

from dominate.tags import *

sys.path.insert(0, ".")
from data import DATASETS


BASEPATH_SALUM = "/Volumes/researchDev$/oulu_dataset/overlapped_defects_with_labels_20200402"
OUTPATH = "output/www/results-salum"

def get_path_method(method, i):
    return f"/tmp/salum/{i:04d}-{method}.png"


def main():
    doc = dominate.document(title="Results on the Salum dataset")
    dataset = DATASETS["salum"]()

    with doc.head:
        pass
        link(rel='stylesheet', href='style.css')
        # script(type='text/javascript', src='script.js')

    with doc:
        for i in range(64):
            name = dataset.data_json[i]["name"]
            with div():
                span(name)
                # Original image
                src = os.path.join(BASEPATH_SALUM, name + ".jpg")
                dst = os.path.join(OUTPATH, "imgs", name + ".jpg")
                if not os.path.exists(dst):
                    shutil.copy(src, dst)
                img(src=f"imgs/{name}.jpg")
                # Greedy
                span("greedy")
                src = get_path_method("greedy-fixed", i)
                dst = os.path.join(OUTPATH, "imgs", f"{name}-greedy-fixed.png")
                if not os.path.exists(dst):
                    shutil.copy(src, dst)
                img(src=f"imgs/{name}-greedy-fixed.png")
                # IPQP
                span("IPFP")
                src = get_path_method("ipqp", i)
                dst = os.path.join(OUTPATH, "imgs", f"{name}-ipqp.png")
                if not os.path.exists(dst):
                    shutil.copy(src, dst)
                img(src=f"imgs/{name}-ipqp.png")


    with open(os.path.join(OUTPATH, "index.html"), "w") as f:
        f.write(str(doc))


if __name__ == "__main__":
    main()
