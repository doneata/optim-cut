import json
import os
import pdb
import random
import shutil
import subprocess
import sys

from enum import IntEnum

import dominate

from dominate import tags
from dominate.util import raw

# from toolz import compose, first, partition_all, second
sys.path.append(".")
from candidates import plot
from constants import U
from data import DATASETS
from grading import get_surface_measure


KEY = "ro98-test-exhaustive-extend-msize-area"
OUTPATH = "output/www/" + KEY

assert os.path.exists(f"output/results/{KEY}.json")

doc = dominate.document(title="Grading the 1998 Red Oak dataset")

os.makedirs(OUTPATH, exist_ok=True)
for f in "imgs static".split():
    os.makedirs(os.path.join(OUTPATH, f), exist_ok=True)

with doc.head:
    tags.meta(**{"content": "text/html;charset=utf-8", "http-equiv": "Content-Type"})
    tags.meta(**{"content": "utf-8", "http-equiv": "encoding"})

    # jQuery
    tags.script(
        type="text/javascript", src="https://code.jquery.com/jquery-3.5.1.min.js"
    )
    # Bootstrap
    tags.link(
        rel="stylesheet",
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css",
    )
    tags.script(
        type="text/javascript",
        src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js",
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo",
        crossorigin="anonymous",
    )
    tags.script(
        type="text/javascript",
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js",
    )
    # My scripts
    tags.link(rel="stylesheet", href="static/style.css")
    # tags.link(rel="shortcut icon", href="favicon.ico", type="image/x-icon")


with open(f"output/results/{KEY}.json", "r") as f:
    data = json.load(f)


dataset = DATASETS["ro98-test"]()


grade_value = {
    "FAS": 0,
    "F1F": 1,
    "SEL": 2,
    "1C": 3,
    "2AC": 4,
    "3AC": 5,
    "NULL": 6,
}


def get_sorted_ids(data):
    board_ids = range(len(dataset))
    grades_true = [dataset.grades[i] for i in board_ids]
    grades_pred = [datum["max-grade"] for datum in data]
    ids_and_grades = zip(board_ids, grades_true, grades_pred)
    ids_and_grades = [(i, x, y) for i, x, y in ids_and_grades if x != y]
    grades_to_values = lambda t: (grade_value[t[1]], grade_value[t[2]])
    ids, _, _ = zip(*sorted(ids_and_grades, key=grades_to_values))
    return ids


ids = get_sorted_ids(data)


with doc:
    with tags.body():
        with tags.div(cls="container"):

            tags.h1("Grading the 1998 Red Oak dataset", cls="mt-5")

            intro = tags.p()
            intro += "The "
            intro += tags.a(
                tags.em("1998 data bank for kiln-dried red oak lumber "),
                href="https://www.fs.usda.gov/treesearch/pubs/3813",
            )
            intro += "consists of 3,487 boards which were graded automatically "
            intro += "and then whose grades were refined manually. "
            intro += "On this web-page we are working on a subset of boards (the 'test' split), "
            intro += "and we are showing those boards that our algorithm graded differently than the groundtruth."

            tags.p("We are using the following settings:")
            with tags.ul():
                suffix = (" + min. size candidates" if "msize" in KEY else "")
                tags.li("candidates: maximal + intersection-based candidates" + suffix)
                tags.li("selection: exhaustive (but stop the search once the desired grade is achieved)")

            tags.p("Confusion matrix:")
            with tags.div():
                tags.img(src=f"{KEY}.png")

            with tags.p():
                tags.span("Color code:")
                with tags.ul():
                    with tags.li():
                        tags.div(cls="box", style="background-color:rgb(222, 184, 135);")
                        tags.span("wood")
                    with tags.li():
                        tags.div(cls="box", style="background-color:rgb(45, 32, 27);")
                        tags.span("defect")
                    with tags.li():
                        tags.div(cls="box", style="background-color:rgb(130, 173, 215);")
                        tags.span("cut")


            with tags.p():
                tags.span("See ")
                tags.a("here", href="https://issuu.com/nhla/docs/2015_rulebook_final")
                tags.span(" for the NHLA rule book.")

            tags.hr()

            SEP = " | "

            for i in ids:

                datum = data[i]
                board = dataset[i]
                board_size = board.shape.to_size(board.resolution)
                board_name = dataset.board_names[i]

                grade_true = dataset.grades[i]
                grade_pred = datum["max-grade"]

                over_or_under = "Overgraded" if grade_value[grade_pred] < grade_value[grade_true] else "Undergraded"

                with tags.div(cls="mt-3"):
                    sm = get_surface_measure(board_size).magnitude

                    with tags.div(style="font-size: 1.2rem; margin-bottom: 5px"):
                        tags.span("Board name: " + board_name + SEP)
                        tags.span("Groundtruth: " + grade_true + SEP)
                        tags.span("Predicted: " + grade_pred + SEP)
                        tags.span(over_or_under + SEP)
                        tags.span(
                            "Board size: {:.2f} in × {:.2f} ft".format(
                                board_size.height.to(U.inch).magnitude,
                                board_size.width.to(U.foot).magnitude,
                            ) + SEP
                        )
                        tags.span("Surface measure: " + str(sm) + " ft")
                        # tags.span("Max. number of cuts allowed: " + str(num_cuts))

                    for side in "FRONT BACK".split():
                        with tags.div(cls="side"):

                            file_name_orig = "{:s}-{:s}.png".format(board_name, side)
                            outpath = os.path.join(OUTPATH, "imgs", file_name_orig)

                            defects = [
                                defect.shape
                                for defect in board.defects
                                if defect.side.name == side
                            ]

                            if not os.path.exists(outpath):
                                plot(
                                    board.shape,
                                    defects,
                                    [],
                                    outpath=outpath,
                                    to_draw_rulers=True,
                                    resolution=board.resolution,
                                )

                            with tags.div():
                                tags.span("side: " + side.lower() + SEP)
                                tags.span("original board")

                            tags.img(src=os.path.join("imgs", file_name_orig))

                            for info in datum["side-data"][side]["grades-info"]:

                                if not info["has-min-size"]:
                                    with tags.div():
                                        is_grade = "☑" if info["is-grade"] else "☐"
                                        tags.span("side: " + side.lower() + SEP)
                                        tags.span("grade: " + info["grade"] + SEP)
                                        tags.span("is grade: " + is_grade + SEP)
                                        tags.span("does not meet minimum size requirements")
                                        continue

                                *_, file_name_pred = info["img-path"].split("/")

                                src = os.path.join(info["img-path"])
                                dst = os.path.join(OUTPATH, "imgs", file_name_pred)

                                if not os.path.exists(dst):
                                    shutil.copy(src, dst)

                                with tags.div():
                                    is_grade = "☑" if info["is-grade"] else "☐"
                                    tags.span("side: " + side.lower() + SEP)
                                    tags.span("grade: " + info["grade"] + SEP)
                                    tags.span("is grade: " + is_grade + SEP)
                                    tags.span("cutting units: " + "{:.2f}".format(info["cutting-units"]) + SEP)
                                    tags.span("req. cutting units: " + "{:.2f}".format(info["required-cutting-units"]))
                                    # tags.span("% SM: " + "{:.2f}".format(100 * info["cutting-units"] / (12 * sm)))

                                tags.img(src=os.path.join("imgs", file_name_pred))

                    tags.hr()

with open(f"{OUTPATH}/index.html", "w") as f:
    f.write(str(doc))

with open(f"{OUTPATH}/static/style.css", "w") as f:
    f.write(""".box {
    float: left;
    height: 15px;
    width: 15px;
    margin-right: 5px;
    border: 0px;
    clear: both;
}

.side + .side {
   margin-top: 35px;
}
""")
