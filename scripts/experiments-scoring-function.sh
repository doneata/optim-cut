for m in greedy-fixed ipqp-alpha-1 ipqp-alpha-10 ipqp-no-dups-alpha-10 ipqp-alpha-10-init:greedy ipqp-alpha-10-init:maxweight ipqp-greedy-alpha-10; do
    for s in 'area' 'area-1.1' 'area-1.2'; do
        python cut.py -d salum -m $m --num-boards 16 --score $s --num-splits 3
    done
done
