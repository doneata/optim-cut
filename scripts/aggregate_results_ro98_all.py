import json
import pdb
import sys

from pprint import pprint

from matplotlib import pyplot as plt

from sklearn.metrics import accuracy_score, confusion_matrix

import seaborn as sns
sns.set_context("paper")

sys.path.append(".")
from data import DATASETS
from grading import Grade1
from grade_ro98 import SUBSET_TO_GRADE

TO_PLOT = []
DATASET_NAME = "ro98-test"
method_name = sys.argv[1]

grades = "FAS F1F SEL 1C 2AC 3AC NULL".split()

path = f"output/results/{DATASET_NAME}-{method_name}-area.json"
with open(path, "r") as f:
    results = json.load(f)

dataset = DATASETS[DATASET_NAME]()

pred = [r["max-grade"] for r in results]
true = [dataset.grades[i] for i in range(len(dataset))]

n_errors = sum(t != p for t, p in zip(true, pred))
n_under = sum(grades.index(t) < grades.index(p) for t, p in zip(true, pred))
n_over = sum(grades.index(t) > grades.index(p) for t, p in zip(true, pred))

print(100 * accuracy_score(true, pred))
print(n_errors, n_over, n_under)

if 'hist-under' in TO_PLOT:
    pdb.set_trace()

if 'conf' in TO_PLOT:
    m = confusion_matrix(true, pred, labels=grades)
    sns.heatmap(m, annot=True, xticklabels=grades, yticklabels=grades)

    # labels, title and ticks
    plt.xlabel('pred')
    plt.ylabel('true')

    plt.tight_layout()
    plt.savefig(f"output/results/confusion-matrix/{DATASET_NAME}-{method_name}-area.png")

# for i in range(len(dataset)):
#     if dataset.grades[i] == "1C" and results[i]["max-grade"] == "FAS":
#         board = dataset.boards[i]
#         pprint(board.shape.to_size(board.resolution))
#         pprint(board.defects)
#         pprint(results[i])
#         print()
