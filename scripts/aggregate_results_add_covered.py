"""Aggregates results and adds the "covered" metric."""
import json
import pdb
import sys

import numpy as np

sys.path.append(".")
from board import Rectangle
from data import DATASETS, split_board


method_name = sys.argv[1]
score_name = sys.argv[2]

NUM_BOARDS = 16
NUM_SPLITS = 3
GRADES = "FAS 1COM 2AB 3ACOM ---".split()

dataset = DATASETS["salum"]()
path = f"output/results/salum-{method_name}-{score_name}.json"
with open(path, "r") as f:
    results = json.load(f)

idx = 0
for i in range(NUM_BOARDS):
    board = dataset[i]
    for j, board1 in enumerate(split_board(board, NUM_SPLITS)):
        area_selected = sum(Rectangle(**r).area() for r in results[idx]["selected-cuts"])
        area_defects = sum(defect.shape.area() for defect in board1.defects)
        area_total = board1.shape.area()
        results[idx]["covered"] = 100 * area_selected / (area_total - area_defects)
        idx += 1

covered = np.mean([r['covered'] for r in results if not r['is-overlap']])
score = np.mean([r['score'] for r in results if not r['is-overlap']])
num_grade = [len(list(1 for r in results if r['max-grade'] == g and not r['is-overlap'])) for g in GRADES]
elapsed_time = np.mean([r['time'] for r in results])
overlap_percentage = 100 * np.mean([r['is-overlap'] for r in results])

print(method_name, score_name, covered, score, overlap_percentage, " ".join(map(str, num_grade)), elapsed_time)
