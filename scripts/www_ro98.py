import json
import os
import pdb
import random
import shutil
import subprocess
import sys

# from itertools import groupby

import dominate

from dominate import tags
from dominate.util import raw

# from toolz import compose, first, partition_all, second
sys.path.append(".")
from constants import U
from data import DATASETS
from grading import get_surface_measure


KEY = sys.argv[1]

assert os.path.exists(f"output/results/{KEY}.json")

# KEY = "ro98-fas-ipqp-alpha-10-area"
# KEY = "ro98-fas-greedy-fixed-area"
# KEY = "ro98-fas-exhaustive-area"
OUTPATH = "output/www/" + KEY
doc = dominate.document(title="Grading the 1998 Red Oak dataset")

os.makedirs(OUTPATH, exist_ok=True)
for f in "imgs static".split():
    os.makedirs(os.path.join(OUTPATH, f), exist_ok=True)

with doc.head:
    tags.meta(**{"content": "text/html;charset=utf-8", "http-equiv": "Content-Type"})
    tags.meta(**{"content": "utf-8", "http-equiv": "encoding"})

    # jQuery
    tags.script(
        type="text/javascript", src="https://code.jquery.com/jquery-3.5.1.min.js"
    )
    # Bootstrap
    tags.link(
        rel="stylesheet",
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css",
    )
    tags.script(
        type="text/javascript",
        src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js",
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo",
        crossorigin="anonymous",
    )
    tags.script(
        type="text/javascript",
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js",
    )
    # My scripts
    tags.link(rel="stylesheet", href="static/style.css")
    # tags.link(rel="shortcut icon", href="favicon.ico", type="image/x-icon")

with open(f"output/results/{KEY}.json", "r") as f:
    data = json.load(f)

dataset = DATASETS["ro98-fas"]()

with doc:
    with tags.body():
        with tags.div(cls="container"):

            tags.h1("Grading the 1998 Red Oak dataset", cls="mt-5")

            intro = tags.p()
            intro += "The "
            intro += tags.a(
                tags.em("1998 data bank for kiln-dried red oak lumber "),
                href="https://www.fs.usda.gov/treesearch/pubs/3813",
            )
            intro += "consists of 3,487 boards which were graded automatically "
            intro += "and then whose grades were refined manually. "
            intro += "On this web-page we are showing boards that are labelled as FAS in the dataset, but which our algorithm graded differently (lower)."

            with tags.p():
                tags.span("Color code:")
                with tags.ul():
                    with tags.li():
                        tags.div(cls="box", style="background-color:rgb(222, 184, 135);")
                        tags.span("wood")
                    with tags.li():
                        tags.div(cls="box", style="background-color:rgb(45, 32, 27);")
                        tags.span("defect")
                    with tags.li():
                        tags.div(cls="box", style="background-color:rgb(130, 173, 215);")
                        tags.span("cut")

            for i, datum in enumerate(data):

                if datum["max-grade"] == "FAS":
                    continue

                # if i > 64:
                #     break

                board = dataset[i]
                board_size = board.shape.to_size(board.resolution)

                with tags.div(cls="mt-3"):
                    sm = get_surface_measure(board_size).magnitude
                    tags.div("Board #" + str(datum["board-index"]))
                    tags.div("Board name: " + str(dataset.board_names[i]))
                    tags.div("Surface measure: " + str(sm) + " ft")
                    tags.div(
                        "Board size: {:.2f} in × {:.2f} in".format(
                            board_size.height.to(U.inch).magnitude,
                            board_size.width.to(U.inch).magnitude,
                        )
                    )
                    tags.div("Groundtruth: FAS")
                    tags.div("Predicted: " + datum["max-grade"])
                    for side in "FRONT BACK".split():
                        *_, file_name = datum["img-paths"][side].split("/")
                        src = os.path.join(datum["img-paths"][side])
                        dst = os.path.join(OUTPATH, "imgs", file_name)

                        # if not os.path.exists(dst):
                        shutil.copy(src, dst)

                        tags.span(
                            side.lower().capitalize()
                            + ": "
                            + datum["max-grades-side"][side]
                        )
                        tags.div("% SM: " + "{:.2f}".format(100 * datum["cutting-units"][side] / (12 * sm)))
                        tags.div("{:.1f} / {:d}".format(
                            datum["cutting-units"][side],
                            datum["required-cutting-units"][side]))
                        tags.img(src=os.path.join("imgs", file_name))

with open(f"{OUTPATH}/index.html", "w") as f:
    f.write(str(doc))

with open(f"{OUTPATH}/static/style.css", "w") as f:
    f.write(""".box {
    float: left;
    height: 15px;
    width: 15px;
    margin-right: 5px;
    border: 0px;
    clear: both;
}""")
