import json
import pdb
import sys

import numpy as np

from toolz import concat

sys.path.append(".")
from data import DATASETS
from grading import get_cutting_units


method_name = sys.argv[1]
score_name = "area"

GRADES = "FAS 1COM 2AB 3ACOM ---".split()
SIDES = ["FRONT", "BACK"]

dataset = DATASETS["ro98-fas"]()

path = f"output/results/ro98-fas-{method_name}-{score_name}.json"
# path = "/tmp/tmp.json"
with open(path, "r") as f:
    results = json.load(f)

def get_board_cutting_units(i):
    board = dataset[i]
    size = board.shape.to_size(board.resolution)
    return get_cutting_units(size)

has_grade = [r['max-grades-side'][s] == 'FAS' for s in SIDES for r in results]
rel_areas = [r['cutting-units'][s] / get_board_cutting_units(i) for s in SIDES for i, r in enumerate(results)]

print("{:.2f} | {:.2f} | {:.2f}".format(
    100 * np.mean(has_grade),
    100 * np.mean(rel_areas),
    100 * np.median(rel_areas),
))
