for s in 'area' 'area-1.1' 'area-1.2'; do
    for m in greedy-fixed ipqp-alpha-1 ipqp-alpha-10 ipqp-no-dups-alpha-10 ipqp-alpha-10-init:greedy ipqp-alpha-10-init:maxweight ipqp-greedy-alpha-10; do
        # python scripts/aggregate_results.py $m $s
        python scripts/aggregate_results_add_covered.py $m $s
    done
done
