import json
import os
import pdb
import random
import shutil
import subprocess
import sys

# from itertools import groupby

import dominate

from dominate import tags
from dominate.util import raw

# from toolz import compose, first, partition_all, second
sys.path.append(".")
from candidates import plot
from constants import U
from data import DATASETS
from grading import get_surface_measure


KEY = "ro98-fas-selection"
OUTPATH = "output/www/" + KEY
doc = dominate.document(title="1998 Red Oak dataset – Grading the FAS boards")

os.makedirs(OUTPATH, exist_ok=True)
for f in "imgs static".split():
    os.makedirs(os.path.join(OUTPATH, f), exist_ok=True)

with doc.head:
    tags.meta(**{"content": "text/html;charset=utf-8", "http-equiv": "Content-Type"})
    tags.meta(**{"content": "utf-8", "http-equiv": "encoding"})

    # jQuery
    tags.script(
        type="text/javascript", src="https://code.jquery.com/jquery-3.5.1.min.js"
    )
    # Bootstrap
    tags.link(
        rel="stylesheet",
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css",
    )
    tags.script(
        type="text/javascript",
        src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js",
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo",
        crossorigin="anonymous",
    )
    tags.script(
        type="text/javascript",
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js",
    )
    # My scripts
    tags.link(rel="stylesheet", href="static/style.css")
    # tags.link(rel="shortcut icon", href="favicon.ico", type="image/x-icon")

# with open(f"output/results/{KEY}.json", "r") as f:
#     data = json.load(f)

SELECTED_BOARDS = "1F 23F 142F 179F 371F 620F"
SELECTED_BOARDS = [(int(''.join(no)), "FRONT" if side == "F" else "BACK") for *no, side in SELECTED_BOARDS.split()]

dataset = DATASETS["ro98-fas"]()

with doc:
    with tags.body():
        with tags.div(cls="container"):

            tags.h1("FAS grading in the 1998 Red Oak dataset", cls="mt-5")

            raw("""
<p>
This page presents schematic illustrations of boards with corresponding defects from the <em>1998 data bank for kiln-dried red oak lumber</em>.
My goal is to understand why these samples were graded FAS.
For further details on the dataset please refer to the <a href="https://www.fs.usda.gov/treesearch/pubs/3813">corresponding paper</a>.
</p>

<p>
Some information on the data:
<ul>
    <li>The wood is kiln dried, which allows for 1/4-inch narrower widths (for FAS this means we are allowed a minimum width of 5.75 inch instead of 6 inch).</li>
    <li>The grading follows the 1998 NHLA rule book. <b>Q</b> Were significant changes made to grading rules since 1998?</li>
    <li>In the samples below I'm showing a single side for each board. As far as I understand, a FAS grade implies that both sides are FAS.</li>
    <li>Light brown <div class="box" style="background-color:rgb(222, 184, 135);"></div> denotes clear wood,
        while dark brown <div class="box" style="background-color:rgb( 45,  32,  27);"></div> denotes defective regions.</li>
    <li>I'm not differentiating between the types of defects (that is, I'm showing all defects with a single color).
        The types of defects are stain, checks, sound knot, unsound knot, wane, split or shake, pith, hole, decay, bark pocket, void.
        <b>Q</b> Should I explicitly indicate the type of defect?</li>
    <li>The horizontal ruler shows feet, while the vertical shows inches.</li>
</ul>
</p>""")

            tags.h2("Selected samples")

            for i, side in SELECTED_BOARDS:

                board = dataset[i]
                board_size = board.shape.to_size(board.resolution)
                board_name = dataset.board_names[i]
                sm = get_surface_measure(board_size).magnitude

                with tags.div(cls="mt-3"):
                    tags.div("Board no.: " + board_name)
                    tags.div(
                        "Board size: {:.2f} in × {:.2f} ft ({:.2f} in)".format(
                            board_size.height.to(U.inch).magnitude,
                            board_size.width.to(U.foot).magnitude,
                            board_size.width.to(U.inch).magnitude,
                        )
                    )
                    tags.div("Surface measure: " + str(sm) + " ft")

                    defects = [
                        defect.shape
                        for defect in board.defects
                        if defect.side.name == side
                    ]

                    file_name = "{:s}_{:s}.png".format(board_name, side)
                    outpath = os.path.join(OUTPATH, "imgs", file_name)

                    plot(
                        board.shape,
                        defects,
                        [],
                        outpath=outpath,
                        to_draw_rulers=True,
                        resolution=board.resolution,
                    )

                    tags.img(src=os.path.join("imgs", file_name))

with open(f"{OUTPATH}/index.html", "w") as f:
    f.write(str(doc))

with open(f"{OUTPATH}/static/style.css", "w") as f:
    f.write(""".box {
    height: 15px;
    width: 15px;
    margin-right: 5px;
    border: 0px;
    display: inline-block;
}""")
