import json
import sys
import pdb

from functools import partial
from toolz.itertoolz import get, join, pluck

sys.path.insert(0, ".")

from data import DATASETS

grade_value = {
    "FAS": 0,
    "F1F": 1,
    "SEL": 2,
    "1C": 3,
    "2AC": 4,
    "3AC": 5,
    "NULL": 6,
}

def merge(datum, solution):
    sol = {
        "cuts": {
            side: list(pluck(
                ["grade", "selected-cuts"],
                solution["side-data"][side.upper()]["grades-info"]))
            for side in SIDES 
        }
    }
    return dict(datum, **sol)

def is_undergraded(datum, solution):
    true = datum["grade"]
    pred = solution["max-grade"]
    return grade_value[true] < grade_value[pred]

def is_overgraded(datum, solution):
    true = datum["grade"]
    pred = solution["max-grade"]
    return grade_value[true] > grade_value[pred]

data = DATASETS["ro98-test"]()
SIDES = {"front", "back"}

with open("output/results/ro98-test-exhaustive-extend-msize-area.json", "r") as f:
    sols = json.load(f)

data_json = [
    {
        "name": data.board_names[i],
        "board": datum.shape.to_dict(),
        "grade": data.grades[i],
        "defects": {
            side: [
                {
                    "rect": defect.shape.to_dict(),
                    "type": defect.type_.name,
                }
                for defect in datum.defects
                if defect.side.name == side.upper()
            ]
            for side in SIDES
        },
    }
    for i, datum in enumerate(data)
]

data_j = join(partial(get, "name"), data_json, partial(get, "board-name"), sols)
data_j = [merge(datum, sol) for datum, sol in data_j if is_overgraded(datum, sol) or is_undergraded(datum, sol)]

with open("../optim-cut-ui/data/ro98test-mistakes.json", "w") as f:
    json.dump(data_j, f, indent=4, sort_keys=True)
