import json
import pdb
import sys

import numpy as np

method_name = sys.argv[1]
score_name = sys.argv[2]

GRADES = "FAS 1COM 2AB 3ACOM ---".split()

path = f"output/results/salum-{method_name}-{score_name}.json"
with open(path, "r") as f:
    results = json.load(f)

score = np.mean([r['score'] for r in results if not r['is-overlap']])
num_grade = [len(list(1 for r in results if r['max-grade'] == g and not r['is-overlap'])) for g in GRADES]
elapsed_time = np.mean([r['time'] for r in results])
overlap_percentage = 100 * np.mean([r['is-overlap'] for r in results])

print(method_name, score_name, score, overlap_percentage, " ".join(map(str, num_grade)), elapsed_time)
