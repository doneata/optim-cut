import json
import pdb
import sys

import numpy as np

from sklearn.metrics import accuracy_score, confusion_matrix

sys.path.append(".")
from grade_ro98 import SUBSET_TO_GRADE


def load_results(path):
    with open(path, "r") as f:
        return json.load(f)


path = "output-ro98/results/ro98-{}-greedy-fixed-area.json"
subsets = SUBSET_TO_GRADE.keys()
grades = "FAS F1F SEL No1COM No2ACOM No3ACOM NULL".split()

y_pred = []
y_true = []

for s in subsets:
    results = load_results(path.format(s))
    grade = SUBSET_TO_GRADE[s]
    y_true.extend([grade for _ in results])
    y_pred.extend([r["max-grade"] for r in results])

y_true = np.array(y_true)
y_pred = np.array(y_pred)

print(accuracy_score(y_true, y_pred))
print(confusion_matrix(y_true, y_pred, labels=grades))
pdb.set_trace()
