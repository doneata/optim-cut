import json
import os
import pdb
import random
import shutil
import subprocess
import sys

# from itertools import groupby

import dominate

from dominate import tags
from dominate.util import raw

# from toolz import compose, first, partition_all, second
sys.path.append(".")
from constants import U
from data import DATASETS
from grading import GRADES, get_surface_measure


KEY1 = "ro98-fas-ipqp-no-dups-alpha-10-extend-inter-constrained-area"
KEY2 = "ro98-fas-exhaustive-extend-inter-area"

assert os.path.exists(f"output/results/{KEY1}.json")
assert os.path.exists(f"output/results/{KEY2}.json")

OUTPATH = "output/www/ro98-fas-ipqp-vs-exhaustive"
doc = dominate.document(title="1998 Red Oak Dataset: IPQP vs. Exhaustive")

os.makedirs(OUTPATH, exist_ok=True)
for f in "imgs static".split():
    os.makedirs(os.path.join(OUTPATH, f), exist_ok=True)

with doc.head:
    tags.meta(**{"content": "text/html;charset=utf-8", "http-equiv": "Content-Type"})
    tags.meta(**{"content": "utf-8", "http-equiv": "encoding"})

    # jQuery
    tags.script(
        type="text/javascript", src="https://code.jquery.com/jquery-3.5.1.min.js"
    )
    # Bootstrap
    tags.link(
        rel="stylesheet",
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css",
    )
    tags.script(
        type="text/javascript",
        src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js",
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo",
        crossorigin="anonymous",
    )
    tags.script(
        type="text/javascript",
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js",
    )
    # My scripts
    tags.link(rel="stylesheet", href="static/style.css")
    # tags.link(rel="shortcut icon", href="favicon.ico", type="image/x-icon")

with open(f"output/results/{KEY1}.json", "r") as f:
    data_ipqp = json.load(f)

with open(f"output/results/{KEY2}.json", "r") as f:
    data_exhaustive = json.load(f)

dataset = DATASETS["ro98-fas"]()
grader = GRADES["FAS"]

with doc:
    with tags.body():
        with tags.div(cls="container"):

            tags.h1("1998 Red Oak Dataset: IPFP vs. Exhaustive", cls="mt-5")

            intro = tags.p()
            intro += "Showing results where the IPFP (also known as IPQP) fails compared to the exhaustive search approach. "
            intro += "Both methods use the same set of candidates: maximal rectangles suplemented with their intersections and differences."

            with tags.p():
                tags.span("Color code:")
                with tags.ul():
                    with tags.li():
                        tags.div(cls="box", style="background-color:rgb(222, 184, 135);")
                        tags.span("wood")
                    with tags.li():
                        tags.div(cls="box", style="background-color:rgb(45, 32, 27);")
                        tags.span("defect")
                    with tags.li():
                        tags.div(cls="box", style="background-color:rgb(130, 173, 215);")
                        tags.span("cut")
                    with tags.li():
                        tags.div(cls="box", style="background-color:rgb(210, 210, 210);")
                        tags.span("unused cut (due to grade restrictions on maximum number of cuts)")

            for i, (datum_i, datum_e) in enumerate(zip(data_ipqp, data_exhaustive)):

                if datum_i["max-grade"] == "FAS":
                    continue

                if datum_i["max-grade"] != "FAS" and datum_e["max-grade"] != "FAS":
                    continue

                assert datum_i["board-index"] == datum_e["board-index"]

                board = dataset[i]
                board_size = board.shape.to_size(board.resolution)
                num_cuts = grader.get_num_cuts(board_size)

                with tags.div(cls="mt-3"):

                    sm = get_surface_measure(board_size).magnitude
                    tags.i("Board #" + str(datum_i["board-index"]))

                    with tags.div():
                        tags.span(
                            "Board size: {:.2f} in × {:.2f} ft | ".format(
                                board_size.height.to(U.inch).magnitude,
                                board_size.width.to(U.foot).magnitude,
                            )
                        )
                        tags.span("Surface measure: " + str(sm) + " ft" + " | ")
                        tags.span("Max. number of cuts allowed: " + str(num_cuts)) #str(datum_i["num-cuts"]))

                    for side in "FRONT BACK".split():

                        if datum_i["max-grades-side"][side] == "FAS":
                            continue

                        if datum_i["max-grades-side"][side] != "FAS" and datum_e["max-grades-side"][side] != "FAS":
                            continue

                        tags.div("Side: " + side)

                        for name, datum in zip("Exhaustive IPQP".split(), [datum_e, datum_i]):

                            *_, file_name = datum["img-paths"][side].split("/")
                            file_name, ext = file_name.split(".")
                            file_name = file_name + "-" + name + "." + ext

                            src = os.path.join(datum["img-paths"][side])
                            dst = os.path.join(OUTPATH, "imgs", file_name)

                            # if not os.path.exists(dst):
                            shutil.copy(src, dst)

                            with tags.div():
                                tags.span("Method: " + name + " | ")
                                tags.span("Grade: " + datum["max-grades-side"][side] + " | ")
                                tags.span("% SM: " + "{:.2f}".format(100 * datum["cutting-units"][side] / (12 * sm)) + " | ")
                                tags.span("{:.1f} / {:d}".format(
                                    datum["cutting-units"][side],
                                    datum["required-cutting-units"][side]))

                            tags.img(src=os.path.join("imgs", file_name))

with open(f"{OUTPATH}/index.html", "w") as f:
    f.write(str(doc))

with open(f"{OUTPATH}/static/style.css", "w") as f:
    f.write(""".box {
    float: left;
    height: 15px;
    width: 15px;
    margin-right: 5px;
    border: 0px;
    clear: both;
}""")
