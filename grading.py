import argparse
import os
import pdb

from abc import ABCMeta, abstractmethod

from enum import IntEnum

from functools import partial

from math import floor

from pint import Quantity  # type: ignore

from typing import List, Optional

from board import Board, Defect, DefectType, Side, Size

from constants import U

from candidates import Rectangle, plot

from defect_limits import DefectLimits, SizeType

from data import DATASETS

from methods import METHODS


def get_surface_measure(size: Size) -> Quantity:
    sm = size.height.to(U.inch) * size.width.to(U.foot)
    return round(sm.magnitude / 12) * U.foot
    # return int(sm.magnitude / 12) * U.foot


def get_cutting_units(size: Size) -> Quantity:
    return (size.height.to(U.inch) * size.width.to(U.foot)).magnitude


def has_two_sides(board: Board) -> bool:
    pass


def filter_defects(board: Board, side: Side) -> Board:
    pass


class Grade(metaclass=ABCMeta):
    def __init__(self, is_kiln_dried=False):
        self.is_kiln_dried = is_kiln_dried

    @property
    @abstractmethod
    def name(self):
        pass

    @property
    @abstractmethod
    def min_height(self):
        pass

    @property
    @abstractmethod
    def min_width(self):
        pass

    @property
    @abstractmethod
    def min_cut_sizes(self) -> List[Size]:
        pass

    @property
    @abstractmethod
    def yield_factor(self) -> int:
        pass

    @property
    @abstractmethod
    def yield_factor_extra(self) -> int:
        pass

    @abstractmethod
    def get_num_cuts(self, size: Size) -> Optional[int]:
        pass

    @abstractmethod
    def allows_extra_cut(self, sm: float) -> bool:
        pass

    @property
    def defect_limits(self) -> List[DefectLimits]:
        return []

    def has_min_board_size(self, size: Size) -> bool:
        if self.is_kiln_dried:
            min_height = self.min_height - 0.25 * U.inch
        else:
            min_height = self.min_height
        return size.height >= min_height and size.width >= self.min_width

    def has_min_cut_size(self, size: Size) -> bool:
        return any(
            size.height >= s.height and size.width >= s.width
            for s in self.min_cut_sizes
        )

    def is_grade_1(
        self,
        board: Board,
        cuts: List[Size],
        defects: List[Defect] = [],
        to_return_extra=False,
    ):

        board_size = board.to_size()
        has_min_size = self.has_min_board_size(board_size)

        cuts = [cut for cut in cuts if self.has_min_cut_size(cut)]
        cuts1 = sorted(cuts, key=get_cutting_units, reverse=True)

        num_cuts = self.get_num_cuts(board_size)

        get_cuts_n = (
            lambda num_cuts: cuts1[:num_cuts] if num_cuts is not None else cuts1
        )
        get_cutting_units_n = lambda num_cuts: sum(
            get_cutting_units(c) for c in get_cuts_n(num_cuts)
        )

        sm = get_surface_measure(board_size)
        get_required_cutting_units = lambda yield_factor: yield_factor * sm.magnitude

        cutting_units = get_cutting_units_n(num_cuts)
        required_cutting_units = get_required_cutting_units(self.yield_factor)

        has_yield = cutting_units >= required_cutting_units
        allows_extra_cut = self.allows_extra_cut(sm)

        # check defect limits
        has_defects_within_limits = all(
            d.within_limits(defects, board)
            for d in self.defect_limits
        )

        if not has_yield and allows_extra_cut and num_cuts is not None and len(cuts) > num_cuts:
            num_cuts = num_cuts + 1
            cutting_units = get_cutting_units_n(num_cuts)
            required_cutting_units = get_required_cutting_units(self.yield_factor_extra)
            has_yield = has_yield or cutting_units >= required_cutting_units

        # if not has_yield and hasattr(self, "check_extra_rule"):
        #     has_yield = has_yield or self.check_extra_rule(board, cuts)

        answer = has_min_size and has_yield

        if to_return_extra:
            extra_info = {
                "num-cuts": num_cuts,
                "cutting-units": cutting_units,
                "required-cutting-units": required_cutting_units,
                "has-min-size": has_min_size,
                "allows-extra-cutting": allows_extra_cut,
            }
            return answer, extra_info
        else:
            return answer


def has_allowed_size_fas_pith(defect_size: Quantity, board_size: Size) -> bool:
    d = defect_size.to(U.inch).magnitude
    b = get_surface_measure(board_size).to(U.foot).magnitude
    return d <= b



def has_allowed_size_fas_knot_hole(defect_size: Quantity, board_size: Size) -> bool:
    d = defect_size.to(U.inch).magnitude
    b = get_surface_measure(board_size).to(U.foot).magnitude
    return d <= b / 3


class FAS(Grade):
    min_height = 6 * U.inch
    min_width = 8 * U.foot
    min_cut_sizes = [
        Size(height=4 * U.inch, width=5 * U.foot),
        Size(height=3 * U.inch, width=7 * U.foot),
    ]
    yield_factor = 10
    yield_factor_extra = 11
    name = "FAS"
    defect_limits = [
        DefectLimits(
            DefectType.PITH,
            aggregation="sum",
            size_type="length",
            has_allowed_size=has_allowed_size_fas_pith,
        ),
        DefectLimits(
            DefectType.WANE,
            aggregation="sum",
            size_type="length",
            has_allowed_size=lambda d, b: d <= b.width,
        ),
        DefectLimits(
            DefectType.SOUND_KNOT,
            aggregation="max",
            size_type="diameter",
            has_allowed_size=has_allowed_size_fas_knot_hole,
        ),
        # {
        #     "defect-type": DefectType.SOUND_KNOT,
        #     "max-size": lambda board: get_surface_measure(board) / 3,
        # },
    ]

    # def __init__(self, *args, **kwargs):
    #     super(FAS, self).__init__(*args, **kwargs)

    def get_num_cuts(self, size: Size) -> int:
        sm = get_surface_measure(size)
        return min(max(floor(sm.magnitude / 4.0), 1), 4)

    def allows_extra_cut(self, sm) -> bool:
        return 6 * U.foot <= sm <= 15 * U.foot

    def check_extra_rule(self, board, cuts):
        """Admits also pieces 6" and wider of 6' to 12' surface measure that
        will yield 11.64/12 (97%) in two clear-face cuttings of any length full
        width of the board.

        """
        board_size = board.to_size()
        sm = get_surface_measure(board_size)
        cuts1 = sorted(cuts, key=get_cutting_units, reverse=True)
        # TODO Check the cuts are full width.
        cutting_units = sum(get_cutting_units(c) for c in cuts1[:2])
        required_cutting_units = 11.64 * sm.magnitude
        return (
            board_size.height >= 6 * U.inch
            and 6 * U.foot <= sm <= 12 * U.foot
            and cutting_units >= required_cutting_units
        )

    # def is_grade_2(
    #     self, board_size: Size, cuts1: List[Size], cuts2: List[Size]
    # ) -> bool:
    #     return self.is_grade_1(board_size, cuts1) and self.is_grade_1(board_size, cuts2)


class FASSelects(FAS):
    min_height = 4 * U.inch
    min_width = 6 * U.foot
    name = "FASSEL"

    # def is_grade_2(self, *args, **kwargs):
    #     assert False


class F1F(FAS):
    name = "F1F"

    # def is_grade_2(
    #     self, board_size: Size, cuts1: List[Size], cuts2: List[Size]
    # ) -> bool:
    #     return (
    #         FAS.is_grade_1(board_size, cuts1) and No1COM.is_grade_1(board_size, cuts2)
    #     ) or (
    #         FAS.is_grade_1(board_size, cuts2) and No1COM.is_grade_1(board_size, cuts1)
    #     )


class Selects(FAS):
    min_cut_sizes = [
        Size(height=4 * U.inch, width=5 * U.foot),
        Size(height=3 * U.inch, width=7 * U.foot),
    ]
    name = "SEL"

    # def is_grade_2(
    #     self, board_size: Size, cuts1: List[Size], cuts2: List[Size]
    # ) -> bool:
    #     return (
    #         FASSelects.is_grade_1(board_size, cuts1)
    #         and No1COM.is_grade_1(board_size, cuts2)
    #     ) or (
    #         FASSelects.is_grade_1(board_size, cuts2)
    #         and No1COM.is_grade_1(board_size, cuts1)
    #     )


class No1COM(Grade):
    min_height = 3 * U.inch
    min_width = 4 * U.foot
    min_cut_sizes = [
        Size(height=4 * U.inch, width=2 * U.foot),
        Size(height=3 * U.inch, width=3 * U.foot),
    ]
    yield_factor = 8
    yield_factor_extra = 9
    name = "No1COM"

    def get_num_cuts(self, size: Size) -> Optional[int]:
        sm = get_surface_measure(size)
        return min(max(floor((sm.magnitude + 1) / 3.0), 1), 5)

    def allows_extra_cut(self, sm) -> bool:
        return 3 * U.foot <= sm <= 10 * U.foot

    # def is_grade_2(
    #     self, board_size: Size, cuts1: List[Size], cuts2: List[Size]
    # ) -> bool:
    #     return self.is_grade_1(board_size, cuts1) and self.is_grade_1(board_size, cuts2)


class No2ACOM(Grade):
    min_height = 3 * U.inch
    min_width = 4 * U.foot
    min_cut_sizes = [Size(height=3 * U.inch, width=2 * U.foot)]
    yield_factor = 6
    yield_factor_extra = 8
    name = "No2ACOM"

    def get_num_cuts(self, size: Size) -> Optional[int]:
        sm = get_surface_measure(size)
        return min(max(floor(sm.magnitude / 2.0), 1), 7)

    def allows_extra_cut(self, sm) -> bool:
        return 2 * U.foot <= sm <= 7 * U.foot

    # def is_grade_2(
    #     self, board_size: Size, cuts1: List[Size], cuts2: List[Size]
    # ) -> bool:
    #     return self.is_grade_1(board_size, cuts1) or self.is_grade_1(board_size, cuts2)


class No3ACOM(Grade):
    min_height = 3 * U.inch
    min_width = 4 * U.foot
    min_cut_sizes = [Size(height=3 * U.inch, width=2 * U.foot)]
    yield_factor = 4
    yield_factor_extra = 4
    name = "No3ACOM"

    def get_num_cuts(self, size: Size) -> Optional[int]:
        return None

    def allows_extra_cut(self, sm) -> bool:
        return False

    # def is_grade_2(
    #     self, board_size: Size, cuts1: List[Size], cuts2: List[Size]
    # ) -> bool:
    #     return self.is_grade_1(board_size, cuts1) or self.is_grade_1(board_size, cuts2)


GRADES = {
    "FAS": FAS(),
    "FAS-SEL": FASSelects(),
    "F1F": F1F(),
    "SEL": Selects(),
    "1COM": No1COM(),
    "2ACOM": No2ACOM(),
    "3ACOM": No3ACOM(),
}
GRADES_1_ORDER = "FAS FAS-SEL 1COM 2ACOM 3ACOM".split()  # Single side
GRADES_2_ORDER = "FAS F1F SEL 1COM 2ACOM 3ACOM".split()  # Both sides


class Grade1(IntEnum):
    FAS = 5
    FASSEL = 4
    No1COM = 3
    No2ACOM = 2
    No3ACOM = 1
    NULL = 0


def get_max_grade_2_from_grades(side_grades) -> str:
    side_grades = [Grade1[g] for g in side_grades]
    if max(side_grades) == Grade1.FAS and min(side_grades) == Grade1.FAS:
        return "FAS"
    elif max(side_grades) == Grade1.FAS and min(side_grades) == Grade1.No1COM:
        return "F1F"
    elif max(side_grades) == Grade1.FASSEL and min(side_grades) >= Grade1.No1COM:
        return "SEL"
    elif min(side_grades) == Grade1.No1COM:
        return "1C"
    elif min(side_grades) == Grade1.No2ACOM:
        return "2AC"
    elif min(side_grades) == Grade1.No3ACOM:
        return "3AC"
    else:
        # print(side_grades)
        # pdb.set_trace()
        return "NULL"


def get_max_grade_1(size, cuts) -> str:
    for g in GRADES_1_ORDER:
        if GRADES[g].is_grade_1(size, cuts):
            return g
    return "---"


# def get_max_grade_2(size, cuts1, cuts2) -> str:
#     for g in GRADES_2_ORDER:
#         if GRADES[g].is_grade_2(size, cuts1, cuts2):
#             return g
#     return "---"
