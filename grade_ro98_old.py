import argparse
import json
import pdb
import os
import warnings

from time import time

from typing import Callable, List, Optional

from functools import wraps
from itertools import count

import numpy as np

from board import Side, U

from candidates import (
    Rectangle,
    get_maximal_rectangles,
    get_maximal_rectangles_selection,
    plot,
)

from data import DATASETS, split_board

from grading import GRADES, GRADES_1_ORDER, get_cutting_units, get_max_grade_2, get_surface_measure

from methods import METHODS

from cut import SCORING_FUNC


def main():
    selected_datasets = [dataset for dataset in DATASETS if dataset.startswith("ro98")]
    parser = argparse.ArgumentParser(description="Cut based on a specified method")
    parser.add_argument(
        "-m", "--method", type=str, choices=METHODS, help="which cutting method to use"
    )
    parser.add_argument(
        "-d",
        "--dataset",
        type=str,
        choices=selected_datasets,
        help="on which data to run",
    )
    parser.add_argument(
        "-n",
        "--num-boards",
        type=int,
        help="how many images from the dataset to process",
    )
    parser.add_argument(
        "-o", "--outdir", default="output", help="where to store the images"
    )
    parser.add_argument("-s", "--min-size", help="minimum size (e.g., 10x3)")
    parser.add_argument(
        "--score", choices=SCORING_FUNC, default="area", help="which score function to use"
    )
    parser.add_argument(
        "--dry-run",
        default=False,
        action="store_true",
        help="run without overwriting previously saved results",
    )
    parser.add_argument(
        "--num-splits", type=int, default=1, help="how many splits to use"
    )
    parser.add_argument(
        "-v", "--verbose", default=0, action="count", help="verbosity level"
    )
    args = parser.parse_args()

    dataset = DATASETS[args.dataset]()
    num_boards = args.num_boards or len(dataset)
    scoring_function = SCORING_FUNC[args.score]

    imgdir = os.path.join(
        args.outdir, "images", f"{args.dataset}-{args.method}-{args.score}"
    )
    os.makedirs(imgdir, exist_ok=True)

    data = []

    # fmt: off
    needs_init_data = (
        args.method.startswith("ipqp")
        and "init:greedy" in args.method.split("-"))
    # fmt: on

    grader = GRADES["FAS"]
    grader.is_kiln_dried = True

    if needs_init_data:
        path = os.path.join(
            args.outdir, "results", f"{args.dataset}-greedy-fixed-{args.score}.json"
        )
        with open(path, "r") as f:
            data_init = json.load(f)
            data_init = {
                (datum["board-index"], datum["split-index"]): [
                    Rectangle(**r) for r in datum["selected-cuts"]
                ]
                for datum in data_init
            }

    for i in range(num_boards):
        board = dataset[i]

        board_size = board.to_size()
        img_paths = {
            side.name: os.path.join(imgdir, f"{i:04d}-{side.name}.png") for side in Side
        }
        selected = {}
        max_grades_1 = {}
        cutting_units = {}
        required_cutting_units = {}

        # if i != 248:
        #     continue

        for side in Side:

            # if side.name != "BACK":
            #     continue

            extra = {}
            defects = [defect.shape for defect in board.defects if defect.side == side]

            extra["min_sizes"] = [
                (
                    (size.height.to(U.inch) / board.resolution).magnitude,
                    (size.width.to(U.inch)  / board.resolution).magnitude,
                )
                for size in grader.min_cut_sizes
            ]

            if needs_init_data:
                extra["init_data"] = data_init[(i, j)]

            if args.method == "manual-selection":
                extra["board_index"] = i
                extra["side"] = side.name

            if args.method.startswith("exhaustive"):
                extra["required_area"] = (
                    grader.yield_factor * get_surface_measure(board_size).magnitude /
                    (board.resolution.to(U.inch).magnitude * board.resolution.to(U.foot).magnitude)
                )
                extra["num_cuts"] = grader.get_num_cuts(board_size)

            def has_min_size(rect):
                return grader.has_min_cut_size(rect.to_size(board.resolution))

            # plot(
            #     board.shape,
            #     defects,
            #     [],
            #     outpath="/tmp/greedy/board-candidates.png",
            #     to_draw_rulers=True,
            #     resolution=board.resolution,
            # )

            selected1 = METHODS[args.method](
                board.shape,
                defects,
                has_min_size=has_min_size,
                scoring_function=scoring_function,
                verbose=args.verbose,
                **extra,
            )

            num_selected = len(selected1)
            is_overlap = any(
                selected1[m].overlaps(selected1[n], type1="semiclosed")
                for m in range(num_selected)
                for n in range(m + 1, num_selected)
            )

            if is_overlap:
                warnings.warn("the selected candidates overlap")
                pdb.set_trace()

            num_cuts = grader.get_num_cuts(board_size)
            print(side.name, "maximum number of cuts", num_cuts)
            selected_all = sorted(selected1, key=scoring_function, reverse=True)
            selected1, selected2 = selected_all[:num_cuts], selected_all[num_cuts:]
            selected[side.name] = selected1

            cutting_units[side.name] = sum(get_cutting_units(s.to_size(board.resolution)) for s in selected1)
            required_cutting_units[side.name] = grader.yield_factor * get_surface_measure(board_size).magnitude

            if not args.dry_run:
                plot(board.shape, defects, selected1, outpath=img_paths[side.name], missing=selected2)

            cuts = [s.to_size(board.resolution) for s in selected1]

            if grader.is_grade_1(board, cuts):
                max_grades_1[side.name] = "FAS"
            else:
                max_grades_1[side.name] = "---"

        if max_grades_1["FRONT"] == "FAS" and max_grades_1["BACK"] == "FAS":
            max_grade_2 = "FAS"
        else:
            max_grade_2 = "---"

        datum = {
            "board-index": i,
            "img-paths": img_paths,
            "cutting-units": cutting_units,
            "required-cutting-units": required_cutting_units,
            "max-grades-side": max_grades_1,
            "max-grade": max_grade_2,
            "num-cuts": num_cuts,
            "selected-cuts": {
                side: [c.to_dict() for c in selected[side]] for side in selected.keys()
            },
        }
        data.append(datum)

        print(json.dumps(datum, indent=True))

    if not args.dry_run:
        path = os.path.join(
            args.outdir, "results", f"{args.dataset}-{args.method}-{args.score}.json"
        )
    else:
        path = "/tmp/tmp.json"

    with open(path, "w") as f:
        json.dump(data, f, indent=True, sort_keys=True)


if __name__ == "__main__":
    main()
