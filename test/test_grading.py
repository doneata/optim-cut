# Examples taken from the following links:
# https://www.ucfp.com/resources/NHLA_Grading_Rules_for_North_American_Hardwoods.pdf
# https://www.esf.edu/wus/documents/IllustratedGradingGuide.pdf

import unittest

from board import Board, Size
from grading import get_surface_measure, FAS, No1COM, No2AB, U


def s(height, width):
    return Size(width * U.foot, height * U.inch)


class TestUtils(unittest.TestCase):
    def test1(self):
        sm = get_surface_measure(Size(width=9 * U.inch, height=8 * U.foot))
        self.assertTrue(sm == 6 * U.foot)


class TestIsFAS(unittest.TestCase):
    g = FAS()

    def test1(self):
        board = s(4, 10)
        cuts = []
        self.assertFalse(self.g.is_grade(board, cuts))

    def test2(self):
        board = s(8.25, 12)
        cuts = [s(8.25, 10)]
        self.assertTrue(self.g.is_grade(board, cuts))

    def test3(self):
        board = s(6, 14)
        cuts = [s(6, 4), s(6, 7)]
        self.assertFalse(self.g.is_grade(board, cuts))

    def test4(self):
        board = s(8, 12)
        cuts = [s(8, 5), s(8, 5)]
        self.assertTrue(self.g.is_grade(board, cuts))


class TestIsNo1Common(unittest.TestCase):
    g = No1COM()

    def test1(self):
        board = s(6, 14)
        cuts = [s(6, 4), s(6, 7)]
        self.assertTrue(self.g.is_grade(board, cuts))

    def test2(self):
        board = s(8, 12)
        cuts = [s(6, 4), s(8, 6)]
        self.assertTrue(self.g.is_grade(board, cuts))

    def test3(self):
        board = s(8, 12)
        cuts = [s(8, 4), s(8, 4)]
        self.assertTrue(self.g.is_grade(board, cuts))


class TestIsNo2AB(unittest.TestCase):
    g = No2AB()

    def test1(self):
        board = s(8, 12)
        cuts = [s(4, 4), s(4, 2), s(4, 7)]
        self.assertTrue(self.g.is_grade(board, cuts))

    def test2(self):
        board = s(8, 12)
        cuts = [s(8, 3), s(8, 2), s(4, 4)]
        self.assertTrue(self.g.is_grade(board, cuts))


if __name__ == "__main__":
    unittest.main()
