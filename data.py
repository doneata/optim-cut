import argparse
import json
import os
import pdb
import random

from abc import ABCMeta, abstractmethod

from typing import cast, Any, Dict, Iterator, List, Tuple

from itertools import count, product

import cairo

import numpy as np

from board import Size

from board import Board, Defect, DefectType, Rectangle, Point, Side

from constants import U


SEED = 1337
random.seed(SEED)
np.random.seed(SEED)


def generate_defects(num_defects: int, rectangle: Rectangle) -> List[Defect]:
    get_x = lambda: random.uniform(rectangle.left, rectangle.right)
    get_y = lambda: random.uniform(rectangle.top, rectangle.bottom)

    w = rectangle.right
    h = rectangle.bottom
    σ = h / 10

    assert rectangle.top == 0 and rectangle.left == 0

    def get_rectangle():
        x = get_x()
        y = get_y()
        dx, = np.random.random(1) * σ + h / 5
        dy, = np.random.random(1) * σ + h / 10
        p1 = Point(x, y)
        p2 = Point(min(w, x + dx), min(h, y + dy))
        return Rectangle.from_points(p1, p2)

    return [Defect(get_rectangle()) for _ in range(num_defects)]


class BoardDataset(metaclass=ABCMeta):
    def __init__(self):
        self.board_names = ["{:04d}".format(i) for i in range(len(self))] 

    @abstractmethod
    def __len__(self) -> int:
        pass

    @abstractmethod
    def __getitem__(self, i: int) -> Board:
        pass


class SyntheticDataset(BoardDataset):
    def __init__(self, num_boards, num_defects):
        super().__init__()
        self.num_boards = num_boards
        self.num_defects = num_defects
        self.shape = Rectangle(0, 0, 1000, 300)
        self.resolution = 1 * U.mm

    def __len__(self):
        return self.num_boards

    def __getitem__(self, i):
        if i > len(self):
            raise IndexError
        defects = generate_defects(self.num_defects, self.shape)
        return Board(self.shape, defects, self.resolution)


class SyntheticDataset2(BoardDataset):
    def __init__(self, num_boards):
        super().__init__()
        self.num_boards = num_boards
        self.resolution = 10 * U.cm  # pixels per cm

        h_fas = 6 * U.inch
        w_fas = 8 * U.foot

        self.h_fas = h_fas.to(U.cm).magnitude
        self.w_fas = w_fas.to(U.cm).magnitude

        self.λ = 7
        self.σ = 20

    def __len__(self):
        return self.num_boards

    def __getitem__(self, i):
        if i > len(self):
            raise IndexError

        h, = np.random.randn(1) * self.σ + self.h_fas
        w, = np.random.randn(1) * self.σ + self.w_fas

        num_defects, = np.random.poisson(self.λ, size=1)

        shape = Rectangle(0, 0, w, h)
        defects = generate_defects(num_defects, shape)

        return Board(shape, defects, self.resolution)


class SalumDataset(BoardDataset):
    def __init__(self, faces):
        super().__init__()
        with open("data/salum.json", "r") as f:
            data_json = json.load(f)  # type: List[Dict[str, Any]]
        # typing.cast(List[Dict[str, Any]], data_json)
        self.data_json = [datum for datum in data_json if datum["name"][-1] in faces]
        self.resolution = 0.7 * U.mm  # pixels per mm

    def __len__(self):
        return len(self.data_json)

    def __getitem__(self, i):
        if i > len(self):
            raise IndexError
        shape = Rectangle(
            0, 0, self.data_json[i]["board-size"][0], self.data_json[i]["board-size"][1]
        )
        defects = [Defect(Rectangle(*args)) for args in self.data_json[i]["defects"]]
        return Board(shape, defects, self.resolution)


class RedOak1998(BoardDataset):
    def __init__(self, grade):
        self.K = 4  # For better vizualizations, enlarge coordinates and decrease resolution
        self.boards, self.board_names = self._parse_file(grade)

    def _parse_file(self, grade) -> Tuple[List[Board], List[str]]:
        def parse_defect(line: str) -> Defect:
            *coords_str, type_, side = line.split()
            coords = [self.K * float(c) for c in coords_str]
            shape = Rectangle(*coords)
            return Defect(shape, DefectType(int(type_)), Side(int(side)))

        def parse_board(lines: List[str]) -> Tuple[Board, str, List[str]]:
            board_name = "{:04d}".format(int(lines[0].strip()))
            resolution = float(lines[1].strip()) * U.inch / self.K
            width, height, _, _ = lines[2].split()
            num_defects, _ = map(int, lines[3].split())
            shape = Rectangle(0, 0, self.K * float(width), self.K * float(height))
            defects = [parse_defect(lines[4 + i]) for i in range(num_defects)]
            return Board(shape, defects, resolution), board_name, lines[4 + num_defects :]

        with open(f"data/rodb98/U{grade}-ALL.DAT", "r") as f:
            lines = f.readlines()

        boards = []
        board_names = []

        while True:
            board, board_name, lines = parse_board(lines)
            boards.append(board)
            board_names.append(board_name)
            if not lines:
                break

        return boards, board_names

    def __len__(self):
        return len(self.boards)

    def __getitem__(self, i):
        return self.boards[i]


class RedOak1998Subset(BoardDataset):
    def __init__(self, filelist):
        grades = "FAS F1F SEL 1C 2AC 3AC".split()
        datasets = {g: RedOak1998(g) for g in grades}
        selected = self._load_filelist(filelist)
        self.boards = self._select_boards(datasets, selected)
        self.board_names, self.grades = zip(*selected)

    def _load_filelist(self, name):
        path = f"data/rodb98/filelists/{name}.txt"
        with open(path, "r") as f:
            names_grades = [line.split() for line in f.readlines()]
        return names_grades

    def _select_boards(self, datasets, selected):
        boards = []
        for name, grade in selected:
            dataset = datasets[grade]
            i = dataset.board_names.index(name)
            boards.append(dataset.boards[i])
        return boards

    def __len__(self):
        return len(self.boards)

    def __getitem__(self, i):
        return self.boards[i]


def split_board(board: Board, num_splits: int) -> Iterator[Board]:
    if num_splits == 1:
        yield board
    else:
        shape = board.shape

        FAS_ASPECT_RATIO = 16
        selected_width = shape.height * FAS_ASPECT_RATIO

        x0 = 0.0
        dx = (shape.width - selected_width) / (num_splits - 1)

        def is_in_board(defect, shape):
            return shape.left <= defect.right and defect.left <= shape.right

        def cut_extra(shape, defect):
            return Rectangle(
                max(defect.left, shape.left),
                defect.top,
                min(defect.right, shape.right),
                defect.bottom,
            )

        for i in range(num_splits):
            x1 = x0 + selected_width
            shape0 = Rectangle(x0, shape.top, x1, shape.bottom)
            shape1 = shape0.translate(-x0, 0)
            defects1 = [
                Defect(
                    shape=cut_extra(shape0, defect.shape).translate(-x0, 0),
                    type_=defect.type_,
                    side=defect.side,
                )
                for defect in board.defects
                if is_in_board(defect.shape, shape0)
            ]
            yield Board(shape1, defects1, board.resolution)
            x0 = x0 + dx


def plot(
    rectangle: Rectangle,
    defects: List[Rectangle],
    candidates: List[Rectangle],
    outpath: str,
    to_show_coordinates: bool = False,
    to_draw_rulers: bool = False,
    resolution = None,
    missing = [],
) -> None:
    PIXEL_SCALE = 1

    def norm_color(*rgb):
        return tuple([v / 255 for v in rgb])

    COLORS = {
        "wood": norm_color(222, 184, 135),
        "defect": norm_color(45, 32, 27),
        "cut": norm_color(130, 173, 215),
        "cut-border": norm_color(7, 57, 126),
        "ruler": norm_color(125, 125, 125),
        "missing": norm_color(210, 210, 210),
    }  # type: Dict[str, Tuple[float, float, float]]

    width = rectangle.right - rectangle.left
    height = rectangle.bottom - rectangle.top

    if to_draw_rulers:
        margin = 45
        δ = 5
    else:
        margin = 0

    # Set up the canvas
    surface = cairo.ImageSurface(
        cairo.FORMAT_ARGB32,
        int((width + margin) * PIXEL_SCALE),
        int((height + margin) * PIXEL_SCALE),
    )
    ctx = cairo.Context(surface)
    ctx.scale(PIXEL_SCALE, PIXEL_SCALE)  # Normalizing the canvas

    # Draw rectangle
    ctx.rectangle(0, 0, width, height)
    ctx.set_source_rgb(*COLORS["wood"])
    ctx.fill()

    # Draw candidates
    ctx.set_line_width(3)

    for r in candidates:
        ctx.save()
        ctx.rectangle(r.left, r.top, r.right - r.left, r.bottom - r.top)
        ctx.set_source_rgba(*COLORS["cut"], alpha=0.8)
        ctx.fill_preserve()
        ctx.set_source_rgba(*COLORS["cut-border"], alpha=0.8)
        ctx.stroke()
        ctx.restore()

    # Draw defects
    for d in defects:

        ctx.save()
        ctx.set_source_rgb(*COLORS["defect"])
        ctx.rectangle(d.left, d.top, d.right - d.left, d.bottom - d.top)
        ctx.fill()

        if to_show_coordinates:
            ctx.select_font_face("Sans")
            ctx.set_font_size(10.15)
            ctx.translate(-0.3, -0.1)
            ctx.set_source_rgb(*COLORS["defect"])
            ctx.translate(d.left, d.top)
            ctx.show_text("({:.1f}, {:.1f}, {:.1f}, {:.1f})".format(d.left, d.top, d.right, d.bottom))

        ctx.restore()

    # Draw missing
    for r in missing:
        ctx.save()
        ctx.set_source_rgb(*COLORS["missing"])
        ctx.rectangle(r.left, r.top, r.right - r.left, r.bottom - r.top)
        ctx.fill()
        ctx.restore()

    if to_draw_rulers:
        ctx.move_to(rectangle.left, rectangle.bottom + δ)
        ctx.line_to(rectangle.right, rectangle.bottom + δ)
        ctx.set_source_rgb(*COLORS["ruler"])
        ctx.set_line_width(4)
        ctx.stroke()

        n_ticks = int(np.ceil(width * resolution.to(U.foot).magnitude))

        for t, i in product(range(n_ticks), range(10)):

            x = (t + i / 10) / resolution.to(U.foot).magnitude
            x = int(x)

            if x > width:
                break

            if i == 0:
                length = 20
            elif i == 5:
                length = 15
            else:
                length = 10

            ctx.move_to(x, rectangle.bottom + δ)
            ctx.rel_line_to(0, length)
            ctx.set_source_rgb(*COLORS["ruler"])
            ctx.set_line_width(3)
            ctx.stroke()

            if i == 0 and t > 0:
                ctx.select_font_face("Sans")
                ctx.set_font_size(20)
                ctx.move_to(x - 7.5, rectangle.bottom + δ + length + 20)
                ctx.set_source_rgb(*COLORS["ruler"])
                ctx.show_text("{:d}".format(t))

        ctx.move_to(rectangle.right + δ, rectangle.top)
        ctx.line_to(rectangle.right + δ, rectangle.bottom)
        ctx.set_source_rgb(*COLORS["ruler"])
        ctx.set_line_width(4)
        ctx.stroke()

        n_ticks = int(np.ceil(height * resolution.to(U.inch).magnitude))

        for t, i in product(range(n_ticks), range(10)):

            y = (t + i / 10) / resolution.to(U.inch).magnitude
            y = int(y)

            if y > height:
                break

            if i == 0:
                length = 10
            else:
                continue

            ctx.move_to(rectangle.right + δ, y)
            ctx.rel_line_to(length, 0)
            ctx.set_source_rgb(*COLORS["ruler"])
            ctx.set_line_width(3)
            ctx.stroke()

            if i == 0 and t > 0:
                ctx.select_font_face("Sans")
                ctx.set_font_size(15)
                ctx.move_to(rectangle.right + δ + length + 5, y + 5)
                ctx.set_source_rgb(*COLORS["ruler"])
                ctx.show_text("{:d}".format(t))

    surface.write_to_png(outpath)


DATASETS = {
    "synth": lambda: SyntheticDataset(16, 5),
    "synth2": lambda: SyntheticDataset2(16),
    "salum": lambda: SalumDataset("p"),
    "ro98-fas": lambda: RedOak1998("FAS"),
    "ro98-f1f": lambda: RedOak1998("F1F"),
    "ro98-sel": lambda: RedOak1998("SEL"),
    "ro98-1c": lambda: RedOak1998("1C"),
    "ro98-2ac": lambda: RedOak1998("2AC"),
    "ro98-3ac": lambda: RedOak1998("3AC"),
    "ro98-test": lambda: RedOak1998Subset("full-test"),
    "ro98-full": lambda: RedOak1998Subset("full"),
}


def main():
    parser = argparse.ArgumentParser(description="Samples from a dataset")
    parser.add_argument(
        "-d", "--dataset", type=str, choices=DATASETS, help="on which data to run"
    )
    parser.add_argument(
        "-n",
        "--num-boards",
        type=int,
        help="how many images from the dataset to process",
    )
    parser.add_argument(
        "--num-splits", type=int, default=1, help="how many splits to use"
    )
    args = parser.parse_args()

    dataset = DATASETS[args.dataset]()

    os.makedirs(os.path.join("/tmp", args.dataset), exist_ok=True)

    for i in range(args.num_boards):
        board = dataset[i]
        for j, board1 in enumerate(split_board(board, args.num_splits)):
            for side in list(Side):
                outpath = os.path.join(
                    "/tmp", args.dataset, f"{i:04d}-{j:d}-{side.name}.png"
                )
                defects = [
                    defect.shape for defect in board1.defects if defect.side == side
                ]
                plot(board1.shape, defects, [], outpath=outpath)
                print(board1.shape.width / board1.shape.height)


if __name__ == "__main__":
    main()
