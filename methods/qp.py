import pdb

from typing import Callable, List

import numpy as np

from cvxopt import matrix, solvers  # type: ignore

from candidates import Rectangle, get_maximal_rectangles, plot


def get_adjacency_matrix(rectangles: List[Rectangle], to_constrain_subparts = False) -> np.ndarray:
    A = np.vstack(
        [np.array([int(r1.overlaps(r2, type1="semiclosed")) for r1 in rectangles]) for r2 in rectangles]
    )

    if to_constrain_subparts:
        for i, r1 in enumerate(rectangles):
            for j, r2 in enumerate(rectangles):
                conds = (
                    r1.top == r2.top and r1.bottom == r2.bottom and r1.left == r2.right and Rectangle(r2.left, r1.top, r1.right, r1.bottom) in rectangles,
                    r1.top == r2.top and r1.bottom == r2.bottom and r1.right == r2.left and Rectangle(r1.left, r1.top, r2.right, r1.bottom) in rectangles,
                    r1.left == r2.left and r1.right == r2.right and r1.top == r2.bottom and Rectangle(r1.left, r2.top, r1.right, r1.bottom) in rectangles,
                    r1.left == r2.left and r1.right == r2.right and r1.bottom == r2.top and Rectangle(r1.left, r1.top, r1.right, r2.bottom) in rectangles,
                )
                if any(conds):
                    A[i, j] = 1

    np.fill_diagonal(A, 0)
    return A


def get_weights(
    rectangles: List[Rectangle], scoring_function: Callable[[Rectangle], float]
) -> np.ndarray:
    return np.array([scoring_function(r) for r in rectangles])


def solve_qp(P, q, α=2):
    n = len(q)
    # obective function: - 1 / 2 α x' P x + q' x
    P = matrix(-1 / 2 * α * P)
    q = matrix(q)
    # inequality constraints: x ≥ 0 or -x ≤ 0
    G = matrix(-np.eye(n))
    h = matrix(0.0, (n, 1))
    # equality constraints: 1' x = 1
    A = matrix(1.0, (1, n))
    b = matrix(1.0)
    sol = solvers.qp(P, q, G, h, A, b)
    pdb.set_trace()


def binarize(x: np.ndarray, τ=0.5) -> np.ndarray:
    return x >= τ
