from functools import partial

from typing import Callable, Dict

from .greedy import greedy1

from .ipqp import ipqp1

from .manual_selection import manual_selection

from .exhaustive import exhaustive

from candidates import (
    generate_candidates_from_overlap_inter,
    generate_candidates_from_overlap_minsize,
)


METHODS = {
    "greedy-fixed": partial(greedy1, candidates_type="fixed"),
    "greedy-fixed-extend-inter": partial(greedy1, candidates_type="fixed", extend_candidates_type="inter"),
    "greedy-fixed-extend-msize": partial(greedy1, candidates_type="fixed", extend_candidates_type="msize"),
    "greedy-dynamic": partial(greedy1, candidates_type="dynamic"),
    "ipqp": ipqp1,
    "ipqp-alpha-1": partial(ipqp1, α=1.0),  # same as `ipqp`
    "ipqp-alpha-10": partial(ipqp1, α=10.0),
    "ipqp-no-dups-alpha-10": partial(ipqp1, α=10.0, to_remove_dups=True),
    "ipqp-no-dups-alpha-10-extend-inter": partial(ipqp1, α=10.0, to_remove_dups=True, extend_candidates_type="inter"),
    "ipqp-no-dups-alpha-10--init:random-extend-inter": partial(ipqp1, α=10.0, to_remove_dups=True, init_type="random", extend_candidates_type="inter"),
    "ipqp-no-dups-alpha-10-extend-msize": partial(ipqp1, α=10.0, to_remove_dups=True, extend_candidates_type="msize"),
    "ipqp-no-dups-alpha-10-extend-inter-constrained": partial(ipqp1, α=10.0, to_remove_dups=True, extend_candidates_type="inter", to_constrain_subparts=True),
    "ipqp-no-dups-alpha-10-init:area-extend-inter-constrained": partial(ipqp1, α=10.0, to_remove_dups=True, init_type="area", extend_candidates_type="inter", to_constrain_subparts=True),
    "ipqp++": partial(ipqp1, α=10.0, to_remove_dups=True, extend_candidates_type="min-size"),
    "ipqp-alpha-10-init:greedy": partial(ipqp1, α=10.0, init_type="greedy"),
    "ipqp-alpha-10-init:maxweight": partial(ipqp1, α=10.0, init_type="maxweight"),
    "ipqp-greedy-alpha-10": partial(ipqp1, α=10.0, to_add_missing=True),
    "manual-selection": manual_selection,
    "exhaustive": exhaustive,
    "exhaustive-extend-inter": partial(exhaustive, extend_candidates_type="inter"),
    "exhaustive-extend-msize": partial(exhaustive, extend_candidates_type="msize"),
}  # type: Dict[str, Callable]
