import pdb

from typing import List, Callable

from itertools import combinations, product

from toolz import concat

from candidates import (
    Rectangle,
    extend_candidates,
    get_maximal_rectangles,
    plot,
)


def exhaustive(
    board: Rectangle,
    defects: List[Rectangle],
    has_min_size: Callable[[Rectangle], bool],
    num_cuts,
    required_areas,
    min_sizes=None,
    extend_candidates_type=None,
    verbose: int = 0,
    stop_early: bool = True,
    **kwargs,
) -> List[Rectangle]:

    candidates = list(get_maximal_rectangles(board, defects))
    candidates = [c for c in candidates if has_min_size(c)]
    candidates = extend_candidates(
        extend_candidates_type, board, defects, candidates, min_sizes, has_min_size
    )
    candidates = list(set(candidates))

    # print(len(candidates))
    # for i, c in enumerate(candidates):
    #     plot(board, defects, [c], outpath=f"/tmp/ec/{i:03d}.png", to_show_coordinates=True)
    # pdb.set_trace()

    best_cs = []  # type: List[Rectangle]
    best_score = 0.0  # type: float

    if num_cuts is None:
        num_cuts = len(candidates)

    for n in range(1, num_cuts + 1):
        for cs in combinations(candidates, n):
            if any(c1.overlaps(c2, "semiclosed") for c1, c2 in combinations(cs, 2)):
                score = 0.0
            else:
                score = sum(c.area() for c in cs)
            if stop_early and score > required_areas[0]:
                return list(cs)
            if score > best_score:
                best_cs = list(cs)
                best_score = score

    # Allow extra cut
    if len(required_areas) == 2:
        for cs in combinations(candidates, num_cuts + 1):
            if any(c1.overlaps(c2, "semiclosed") for c1, c2 in combinations(cs, 2)):
                score = 0
            else:
                score = sum(c.area() for c in cs)
            if stop_early and score > required_areas[1]:
                return list(cs)
            if score > best_score:
                best_cs = list(cs)
                best_score = score

    return best_cs
