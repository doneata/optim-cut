import pdb
import os

from typing import Callable, List, Optional

from toolz import concat

from itertools import count

import numpy as np

from scipy.optimize import line_search  # type: ignore

from candidates import (
    Rectangle,
    extend_candidates,
    get_maximal_rectangles,
    get_maximal_rectangles_selection,
    plot,
)

from .qp import binarize, get_adjacency_matrix, get_weights


def ipqp1(
    board: Rectangle,
    defects: List[Rectangle],
    has_min_size: Callable[[Rectangle], bool],
    scoring_function: Callable[[Rectangle], float],
    num_cuts: Optional[int] = None,
    α: float = 1.0,
    init_type: str = "uniform",
    init_data: Optional[List[Rectangle]] = None,
    to_add_missing: bool = False,
    to_remove_dups: bool = False,
    to_constrain_subparts: bool = False,
    extend_candidates_type=None,
    min_sizes = None,
    verbose: int = 0,
) -> List[Rectangle]:

    δ = 1e-5
    η = 0.0
    max_iter = 1000

    candidates = list(get_maximal_rectangles(board, defects))
    candidates = [c for c in candidates if has_min_size(c)]
    candidates = extend_candidates(
        extend_candidates_type, board, defects, candidates, min_sizes, has_min_size
    )
    if to_remove_dups:
        candidates = list(set(candidates))

    if not candidates:
        return []

    # print(len(candidates))
    # for i, c in enumerate(candidates):
    #     plot(board, defects, [c], outpath=f"/tmp/c-{i:03d}.png", to_show_coordinates=True)

    if not candidates:
        return []

    A = get_adjacency_matrix(candidates, to_constrain_subparts)
    w = get_weights(candidates, scoring_function)

    x_star = np.zeros(w.shape)

    if init_type == "uniform":
        y_prev = np.ones(w.shape) / len(w.shape)
    elif init_type == "random":
        y_prev = np.random.rand(*w.shape)
    elif init_type == "maxweight":
        y_prev = np.zeros(w.shape)
        y_prev[np.argmax(w)] = 1.0
    elif init_type == "area":
        y_prev = w / np.sum(w)
    elif init_type == "greedy":
        assert init_data is not None
        idxs = [candidates.index(i) for i in init_data]
        y_prev = np.zeros(w.shape)
        y_prev[idxs] = 1
        assert y_prev.sum() == len(idxs)

    # print(y_prev)

    if verbose >= 1:
        header = "{:4s} {:>7s} {:>10s} {:>10s} {:>10s}"
        names = "iter η f(y_new) f(x_hat) f(x_star)".split()
        print(header.format(*names))

    if verbose >= 2:
        plot(board, defects, [], outpath="/tmp/ipqp/board.png")

    f = lambda x: w.T @ x - 1 / 2 * α * x.T @ A @ x
    g = lambda x: w - α * A @ x

    for t in range(max_iter):
        d = g(y_prev)
        x_hat = binarize(d, τ=0).astype(float)

        # Q Should we avoid the trivial solution?
        # if not any(x_hat == 1):
        #     x_hat[np.argmax(d)] = 1.0

        if f(x_hat) >= f(y_prev):
            y_new = x_hat
        else:
            num = (w - α * A @ y_prev).T @ (x_hat - y_prev)
            den = α * (x_hat - y_prev).T @ A @ (x_hat - y_prev)
            η = min(max(num / den, 0), 1)
            y_new = y_prev + η * (x_hat - y_prev)

        if f(x_hat) >= f(x_star):
            x_star = x_hat

        if np.linalg.norm(y_new - y_prev) < δ:
            break

        y_prev = y_new

        if verbose >= 1:
            print(
                f"{t:04d} {η:.5f} {f(x_hat):+10.2f} {f(y_new):+10.2f} {f(x_star):+10.2f}"
            )

    selected = [candidates[i] for i, val in enumerate(x_star) if val]

    if to_add_missing:
        for i in count(0):
            candidates = [
                c for c in candidates if all(not c.overlaps(s) for s in selected)
            ]
            if not candidates:
                break
            candidate = max(candidates, key=scoring_function)
            selected.append(candidate)
            if verbose >= 1:
                print(i, len(selected), len(candidates), candidate)

    if verbose >= 2:
        plot(board, defects, selected, outpath=f"/tmp/ipqp/selected.png")

    return selected


def ipqp1_line_search(
    board: Rectangle,
    defects: List[Rectangle],
    has_min_size: Callable[[Rectangle], bool],
    scoring_function: Callable[[Rectangle], float],
    num_cuts: Optional[int] = None,
    α: float = 1.0,
    verbose: int = 0,
) -> List[Rectangle]:

    δ = 1e-5
    max_iter = 1000

    candidates = list(get_maximal_rectangles(board, defects))
    candidates = [c for c in candidates if has_min_size(c)]

    C = get_adjacency_matrix(candidates)
    w = get_weights(candidates, scoring_function)
    x = np.ones(w.shape) / len(w)

    if verbose >= 2:
        plot(board, defects, [], outpath="/tmp/ipqp/board.png")

    # to minimize
    obj = lambda x: -w.T @ x + 1 / 2 * α * x.T @ C @ x
    grd = lambda x: -w + α * C @ x

    bs = []

    for i in range(max_iter):
        d = -grd(x)
        b = binarize(d, τ=0)

        λ, *_ = line_search(obj, grd, x, b - x)
        λ = min(max(λ, 0), 1)
        bs.append(b)

        if λ is None:
            break

        x_new = x + λ * (b - x)

        if np.linalg.norm(x_new - x) < δ:
            break

        x = x_new

        if verbose >= 1:
            print(f"{i:03d} {λ:.5f} {obj(b):+10.2f} {obj(x):+10.2f}")

    b_star = min(bs, key=obj)
    selected = [candidates[i] for i, val in enumerate(b) if val]

    if verbose >= 2:
        plot(board, defects, selected, outpath=f"/tmp/ipqp/selected-{i:03d}.png")

    return selected
