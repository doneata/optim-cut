import pdb
import os

from typing import Callable, List, Optional

from itertools import count

import numpy as np

from candidates import (
    Rectangle,
    extend_candidates,
    get_maximal_rectangles,
    get_maximal_rectangles_selection,
    plot,
)


def update_candidates(candidates, s):
    candidates1 = []
    for c in candidates:
        cs = []

        if c.overlaps(s):

            xs = c.to_interval_x() - s.to_interval_x()
            ys = c.to_interval_y() - s.to_interval_y()

            for i in list(xs):
                if not i.empty:
                    r = Rectangle(i.lower, c.top, i.upper, c.bottom)
                    cs.append(r)
                    if s.overlaps(r):
                        a = r.to_interval_x() & s.to_interval_x()
                        b = r.to_interval_y() & s.to_interval_y()
                        assert ((a.upper - a.lower) * (b.upper - b.lower)) == 0
                        if r.area() == 0:
                            pdb.set_trace()

            for i in list(ys):
                if not i.empty:
                    r = Rectangle(c.left, i.lower, c.right, i.upper)
                    cs.append(r)
                    if s.overlaps(r):
                        a = r.to_interval_x() & s.to_interval_x()
                        b = r.to_interval_y() & s.to_interval_y()
                        assert ((a.upper - a.lower) * (b.upper - b.lower)) == 0
                        if r.area() == 0:
                            pdb.set_trace()

        else:
            cs = [c]
        candidates1.extend(cs)
    return candidates1


def greedy1(
    board: Rectangle,
    defects: List[Rectangle],
    has_min_size: Callable[[Rectangle], bool],
    scoring_function: Callable[[Rectangle], float],
    candidates_type: str,
    num_cuts: Optional[int] = None,
    extend_candidates_type=None,
    min_sizes = None,
    verbose: int = 0,
) -> List[Rectangle]:
    selected = []  # type: List[Rectangle]

    if verbose >= 2:
        plot(board, defects, [], outpath="/tmp/greedy/board.png")

    candidates = list(get_maximal_rectangles(board, defects))
    candidates = [c for c in candidates if has_min_size(c)]
    # TODO Add option to extend candidates
    candidates = extend_candidates(
        extend_candidates_type, board, defects, candidates, min_sizes, has_min_size
    )
    candidates = list(set(candidates))

    n_candidates = len(candidates)

    if verbose >= 2:
        plot(board, defects, candidates, outpath="/tmp/greedy/board-candidates.png", to_show_coordinates=True)

    if verbose >= 3:
        for i, c in enumerate(candidates):
            plot(board, defects, [c], outpath=f"/tmp/greedy/board-candidate-{i:03d}.png", to_show_coordinates=False)

    for i in count(0):
        # DEBUG
        # candidates = []
        # for t in "1a 1b 2a 2b 2c 2d 3a 3b 3c 3d 4a".split():
        #     if i == 11 and t == "4a":
        #         pdb.set_trace()
        #     cs = get_maximal_rectangles_selection(rectangle, defects + selected, types=[t])
        #     candidates += cs
        #     print(t, len(cs))

        if candidates_type == "fixed":
            candidates = [
                c for c in candidates if all(not c.overlaps(s) for s in selected)
            ]
        else:
            candidates = list(get_maximal_rectangles(board, defects + selected))
            candidates = [c for c in candidates if has_min_size(c)]

        if not candidates:
            break

        candidate = max(candidates, key=scoring_function)
        selected.append(candidate)

        if num_cuts is not None and i > num_cuts:
            break

        if verbose >= 1:
            # DEBUG
            print(i)
            print(len(selected), len(candidates), candidate)
            print(", ".join("{:.2f}".format(scoring_function(c)) for c in candidates))
            print()

        if verbose >= 2:
            plot(board, defects, selected, outpath=f"/tmp/greedy/selected-{i:03d}.png")
            plot(
                board,
                defects,
                candidates,
                outpath=f"/tmp/greedy/candidates-{i:03d}.png",
            )

    return selected
