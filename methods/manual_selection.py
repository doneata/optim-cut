from typing import Dict, List, Literal

from board import Rectangle


Side = Literal["FRONT", "BACK"]


def manual_selection(board_shape, defects, board_index: int, side: Side, **kwargs):
    SELECTION = {
        0: {
            "FRONT": [
                Rectangle(0, 32, 960, 112),
                Rectangle(960, 4, 3076, 128),
            ],
            "BACK": [
            ],
        },
        180: {
            "FRONT": [
                Rectangle(0, 4, 1350, 52),
                Rectangle(0, 52, 1350, 104),
                Rectangle(1350, 28, 2700, 96),
            ],
            "BACK": [
            ],
        },
        241: {
            "FRONT": [
                # Rectangle(0, 4, 1764, 84),
                # Rectangle(0, 84, 2004, 140),
                Rectangle(0, 4, 1764, 100),
                Rectangle(156, 100, 2316, 152),
            ],
            "BACK": [
                Rectangle(156, 4, 2316, 152),
            ],
        },
    }  # type: Dict[int, Dict[Side, List[Rectangle]]]
    return SELECTION[board_index][side]
