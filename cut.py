import argparse
import json
import pdb
import os
import warnings

from time import time

from typing import Callable, List, Optional

from functools import wraps
from itertools import count

import numpy as np

from candidates import (
    Rectangle,
    get_maximal_rectangles,
    get_maximal_rectangles_selection,
    plot,
)

from data import DATASETS, split_board

from grading import get_max_grade_1

from methods import METHODS


SCORING_FUNC = {
    "area": lambda b: b.area() / 10 ** 6,
    "area-1.1": lambda b: b.area() ** 1.1 / 10 ** 6,
    "area-1.2": lambda b: b.area() ** 1.2 / 10 ** 6,
}


def timer(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        start = time()
        result = f(*args, **kwargs)
        end = time()
        elapsed_time = end - start
        return result, elapsed_time

    return wrapper


def main():
    parser = argparse.ArgumentParser(description="Cut based on a specified method")
    parser.add_argument(
        "-m", "--method", type=str, choices=METHODS, help="which cutting method to use"
    )
    parser.add_argument(
        "-d", "--dataset", type=str, choices=DATASETS, help="on which data to run"
    )
    parser.add_argument(
        "-n",
        "--num-boards",
        type=int,
        help="how many images from the dataset to process",
    )
    parser.add_argument(
        "-o", "--outdir", default="output", help="where to store the images"
    )
    parser.add_argument("-s", "--min-size", help="minimum size (e.g., 10x3)")
    parser.add_argument(
        "--score",
        choices=SCORING_FUNC,
        default="area",
        help="which score function to use",
    )
    parser.add_argument(
        "--dry-run",
        default=False,
        action="store_true",
        help="run without overwriting previously saved results",
    )
    parser.add_argument(
        "--num-splits", type=int, default=1, help="how many splits to use"
    )
    parser.add_argument(
        "-v", "--verbose", default=0, action="count", help="verbosity level"
    )
    args = parser.parse_args()

    timed_cut_method = timer(METHODS[args.method])
    dataset = DATASETS[args.dataset]()
    scoring_function = SCORING_FUNC[args.score]
    num_boards = args.num_boards or len(dataset)

    if args.min_size:
        width, height = map(float, args.min_size.split("x"))
    else:
        width, height = 0, 0

    def has_min_size(rect: Rectangle):
        cond1 = rect.width >= width and rect.height >= height
        cond2 = rect.width >= height and rect.height >= width
        return not args.min_size or cond1 or cond2

    imgdir = os.path.join(
        args.outdir, "images", f"{args.dataset}-{args.method}-{args.score}"
    )
    os.makedirs(imgdir, exist_ok=True)

    data = []

    # fmt: off
    needs_init_data = (
        args.method.startswith("ipqp")
        and "init:greedy" in args.method.split("-"))
    # fmt: on

    if needs_init_data:
        path = os.path.join(
            args.outdir, "results", f"{args.dataset}-greedy-fixed-{args.score}.json"
        )
        with open(path, "r") as f:
            data_init = json.load(f)
            data_init = {
                (datum["board-index"], datum["split-index"]): [
                    Rectangle(**r) for r in datum["selected-cuts"]
                ]
                for datum in data_init
            }

    for i in range(num_boards):
        board = dataset[i]
        for j, board1 in enumerate(split_board(board, args.num_splits)):

            extra = {}
            defects = [defect.shape for defect in board1.defects]

            if needs_init_data:
                extra["init_data"] = data_init[(i, j)]

            selected, elapsed_time = timed_cut_method(
                board1.shape,
                defects,
                has_min_size=has_min_size,
                scoring_function=scoring_function,
                verbose=args.verbose,
                **extra,
            )

            num_selected = len(selected)
            is_overlap = any(
                selected[m].overlaps(selected[n], type1="semiclosed")
                for m in range(num_selected)
                for n in range(m + 1, num_selected)
            )

            if is_overlap:
                warnings.warn("the selected candidates overlap")

            max_grade = get_max_grade_1(
                board1.to_size(),
                [s.to_size(board1.resolution) for s in selected],
            )

            imgpath = os.path.join(imgdir, f"{i:04d}-{j}.png")

            if not args.dry_run:
                plot(board1.shape, defects, selected, outpath=imgpath)

            area_selected = sum(r.area() for r in selected)
            area_defects = sum(r.area() for r in defects)
            area_total = board1.shape.area()

            datum = {
                "board-index": i,
                "split-index": j,
                "num-board-splits": args.num_splits,
                "img-path": imgpath,
                "max-grade": max_grade,
                "score": sum(scoring_function(r) for r in selected),
                "time": elapsed_time,
                "is-overlap": is_overlap,
                "selected-cuts": [s.to_dict() for s in selected],
                "covered": 100 * area_selected / (area_total - area_defects),
            }
            data.append(datum)

            print(json.dumps(datum, indent=True))

    if not args.dry_run:
        path = os.path.join(
            args.outdir, "results", f"{args.dataset}-{args.method}-{args.score}.json"
        )
    else:
        path = "/tmp/tmp.json"

    with open(path, "w") as f:
        json.dump(data, f, indent=True, sort_keys=True)


if __name__ == "__main__":
    main()
