import argparse
import json
import pdb
import os
import time
import warnings

from typing import Callable, List, Optional

from functools import wraps
from itertools import count

import numpy as np

from board import Side, U

from candidates import (
    Rectangle,
    get_maximal_rectangles,
    get_maximal_rectangles_selection,
    plot,
)

from data import DATASETS, split_board

from grading import (
    GRADES,
    GRADES_1_ORDER,
    get_cutting_units,
    get_max_grade_2,
    get_max_grade_2_from_grades,
    get_surface_measure,
)

from methods import METHODS

from cut import SCORING_FUNC


GRADE_NAMES = {
    "fas": "FAS",
    "f1f": "F1F",
    "sel": "SEL",
    "1c": "1COM",
    "2ac": "2ACOM",
    "3ac": "3ACOM",
}


def main():
    selected_datasets = [dataset for dataset in DATASETS if dataset.startswith("ro98")]
    parser = argparse.ArgumentParser(description="Cut based on a specified method")
    parser.add_argument(
        "-m", "--method", type=str, choices=METHODS, help="which cutting method to use"
    )
    parser.add_argument(
        "-g",
        "--grade",
        type=str,
        choices=GRADE_NAMES,
        help="for which grade to run",
    )
    parser.add_argument(
        "-n",
        "--num-boards",
        type=int,
        help="how many images from the dataset to process",
    )
    parser.add_argument(
        "-o", "--outdir", default="output", help="where to store the images"
    )
    parser.add_argument("-s", "--min-size", help="minimum size (e.g., 10x3)")
    parser.add_argument(
        "--score",
        choices=SCORING_FUNC,
        default="area",
        help="which score function to use",
    )
    parser.add_argument(
        "--dry-run",
        default=False,
        action="store_true",
        help="run without overwriting previously saved results",
    )
    parser.add_argument(
        "--num-splits", type=int, default=1, help="how many splits to use"
    )
    parser.add_argument(
        "-v", "--verbose", default=0, action="count", help="verbosity level"
    )
    args = parser.parse_args()

    dataset_name = "ro98-" + args.grade
    dataset = DATASETS[dataset_name]()
    num_boards = args.num_boards or len(dataset)
    scoring_function = SCORING_FUNC[args.score]

    key = f"cut-{dataset_name}-{args.method}-{args.score}"

    imgdir = os.path.join(args.outdir, "images", key)
    os.makedirs(imgdir, exist_ok=True)

    data = []

    # fmt: off
    needs_init_data = (
        args.method.startswith("ipqp")
        and "init:greedy" in args.method.split("-"))
    # fmt: on

    grader = GRADES[GRADE_NAMES[args.grade]]
    grader.is_kiln_dried = True

    if needs_init_data:
        path = os.path.join(
            args.outdir, "results", f"{args.dataset}-greedy-fixed-{args.score}.json"
        )
        with open(path, "r") as f:
            data_init = json.load(f)
            data_init = {
                (datum["board-index"], datum["split-index"]): [
                    Rectangle(**r) for r in datum["selected-cuts"]
                ]
                for datum in data_init
            }

    for i in range(num_boards):
        board = dataset[i]
        board_size = board.to_size()

        data_sides = {}

        for side in Side:

            extra = {}
            defects = [defect.shape for defect in board.defects if defect.side == side]

            if needs_init_data:
                extra["init_data"] = data_init[(i, j)]

            if args.method == "manual-selection":
                extra["board_index"] = i
                extra["side"] = side.name

            if args.method.startswith("exhaustive"):
                extra["stop_early"] = False

            extra["min_sizes"] = [
                (
                    (size.height.to(U.inch) / board.resolution).magnitude,
                    (size.width.to(U.inch)  / board.resolution).magnitude,
                )
                for size in grader.min_cut_sizes
            ]

            if args.method.startswith("exhaustive"):
                extra["required_area"] = (
                    grader.yield_factor * get_surface_measure(board_size).magnitude /
                    (board.resolution.to(U.inch).magnitude * board.resolution.to(U.foot).magnitude)
                )
                extra["num_cuts"] = grader.get_num_cuts(board_size)

            def has_min_size(rect):
                return grader.has_min_cut_size(rect.to_size(board.resolution))

            time_s = time.time()
            selected1 = METHODS[args.method](
                board.shape,
                defects,
                has_min_size=has_min_size,
                scoring_function=scoring_function,
                verbose=args.verbose,
                **extra,
            )
            time_e = time.time()

            cuts = [s.to_size(board.resolution) for s in selected1]
            has_grade, info = grader.is_grade_1(board, cuts, to_return_extra=True)

            num_selected = len(selected1)
            is_overlap = any(
                selected1[m].overlaps(selected1[n], type1="semiclosed")
                for m in range(num_selected)
                for n in range(m + 1, num_selected)
            )

            if is_overlap:
                warnings.warn("the selected candidates overlap")
                pdb.set_trace()

            img_path = os.path.join(imgdir, f"{i:04d}-{side.name}.png")

            if not args.dry_run:
                plot(board.shape, defects, selected1, outpath=img_path)

            num_cuts = info["num-cuts"]
            selected_cuts = sorted(selected1, key=lambda s: s.area(), reverse=True)
            selected_cuts = selected_cuts[:num_cuts]
            selected_cuts = [s.to_dict() for s in selected_cuts]
            data_sides[side.name] = {
                "grade": grader.name,
                "img-path": img_path,
                "is-overlap": is_overlap,
                "is-grade": has_grade,
                "selected-cuts": selected_cuts,
                "time": time_e - time_s,
            }

        datum = {
            "board-index": i,
            "board-name": dataset.board_names[i],
            "side-data": data_sides,
        }
        data.append(datum)
        # pdb.set_trace()

        print(json.dumps(datum, indent=True))

    if not args.dry_run:
        path = os.path.join(
            args.outdir, "results", f"{key}.json"
        )
    else:
        path = "/tmp/tmp.json"

    with open(path, "w") as f:
        json.dump(data, f, indent=True, sort_keys=True)


if __name__ == "__main__":
    main()
